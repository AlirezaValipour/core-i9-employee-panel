import React from "react";
import * as Icon from "react-feather";
import { getCurrentEmployee } from "../core/Services/httpServices";
const employee = getCurrentEmployee();
console.log("employee", employee);
const navigationConfig =
  employee.role === "admin"
    ? [
        {
          id: "dashboard",
          title: "داشبورد",
          type: "item",
          icon: <Icon.Home size={20} />,
          permissions: ["admin", "editor"],
          navLink: "/dashboard",
        },
        {
          id: "dropdown",
          title: "دوره ها",
          type: "collapse",
          icon: <Icon.Book size={20} />,
          // badge: "warning",
          // badgeText: "2",
          children: [
            {
              id: "allCourses",
              title: "لیست دوره ها",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allCourses",
            },
            {
              id: "addCourse",
              title: "اضافه کردن دوره",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin"],
              navLink: "/addCourse",
            },
          ],
        },

        {
          id: "dropdown2",
          title: "اساتید",
          type: "collapse",
          icon: <Icon.Users size={20} />,
          // badge: "warning",
          // badgeText: "2",
          children: [
            {
              id: "allTeachers",
              title: "لیست اساتید",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allTeachers",
            },
          ],
        },
        {
          id: "dropdown3",
          title: "دانشجویان",
          type: "collapse",
          icon: <Icon.Users size={20} />,
          // badge: "warning",
          // badgeText: "2",
          children: [
            {
              id: "allstudents",
              title: "لیست دانشجویان",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allstudents",
            },
            {
              id: "addStudentToTerm",
              title: "حذف و اضافه",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin"],
              navLink: "/addStudentToTerm",
            },
          ],
        },

        {
          id: "dropdown4",
          title: "ترم ها",
          type: "collapse",
          icon: <Icon.BookOpen size={20} />,

          children: [
            {
              id: "allTerms",
              title: "لیست ترم ها",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allTerms",
            },
            {
              id: "addTerm",
              title: "اضافه کردن ترم",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin"],
              navLink: "/addTerm",
            },
          ],
        },

        {
          id: "dropdown5",
          title: "اخبار و مقالات",
          type: "collapse",
          icon: <Icon.Globe size={20} />,

          children: [
            {
              id: "allNews",
              title: "لیست اخبار",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allNews",
            },
            {
              id: "allArticles",
              title: "لیست مقالات",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allArticles",
            },
            {
              id: "addNews",
              title: "افزودن خبر و مقاله",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin"],
              navLink: "/addNewsArticle",
            },
          ],
        },

        {
          id: "dropdown6",
          title: "کامنت ها",
          type: "collapse",
          icon: <Icon.MessageCircle size={20} />,

          children: [
            {
              id: "allComments",
              title: "لیست کامنت ها",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allComments",
            },
          ],
        },
      ]
 
  :[
    {
      id:"dashboard",
      title: "داشبورد",
      type:"item",
      icon: <Icon.Home size={20} />,
      permissions: ["admin", "editor"],
      navLink: "/dashboard",
    },
        {
          id: "dropdown",
          title: "دوره ها",
          type: "collapse",
          icon: <Icon.Book size={20} />,
          // badge: "warning",
          // badgeText: "2",
          children: [
            {
              id: "allCourses",
              title: "لیست دوره ها",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allCourses",
            },
            {
              id: "addCourse",
              title: "اضافه کردن دوره",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin"],
              navLink: "/addCourse",
            },
          ],
        },
        {
          id: "dropdown5",
          title: "اخبار و مقالات",
          type: "collapse",
          icon: <Icon.Globe size={20} />,

          children: [
            {
              id: "allNews",
              title: "لیست اخبار",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allNews",
            },
            {
              id: "allArticles",
              title: "لیست مقالات",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allArticles",
            },
            {
              id: "addNews",
              title: "افزودن خبر و مقاله",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin"],
              navLink: "/addNewsArticle",
            },
          ],
        },

        {
          id: "dropdown6",
          title: "کامنت ها",
          type: "collapse",
          icon: <Icon.MessageCircle size={20} />,

          children: [
            {
              id: "allComments",
              title: "لیست کامنت ها",
              type: "item",
              icon: <Icon.Circle size={12} />,
              permissions: ["admin", "editor"],
              navLink: "/allComments",
            },
          ],
        },
      ];
   

  
  
  






export default navigationConfig;
