import React, { Suspense, lazy} from "react";
import { Router, Switch, Route } from "react-router-dom";
import { history } from "./history";
import Spinner from "./components/@vuexy/spinner/Loading-spinner";
import { ContextLayout } from "./core/utils/context/Layout";
import {EmployeeProvider} from "./core/Context/context";
import {getCurrentEmployee} from "./core/Services/httpServices";
import VerticalLayout from "./layouts/VerticalLayout";

const Home = lazy(() => import("./screens/Home"));

const Page2 = lazy(() => import("./screens/Page2"));

const login = lazy(() => import("./screens/login/Login"));

const Dashboard = lazy(() => import("./screens/Dashboard/Dashboard"));

const AllStudentsPage = lazy(() => import("./screens/Students/AllStudents/AllStudents"));
const EditStudents = lazy(() => import("./screens/Students/EditStudents/EditStudents"));
const DeleteStudents = lazy(() =>
  import("./screens/Students/DeleteStudents/DeleteStudents")
);
const ActiveStudent = lazy(() =>
  import("./screens/Students/ActiveStudent/ActiveStudent")
);
const DeactiveStudent = lazy(() =>
  import("./screens/Students/DeactiveStudent/DeactiveStudent")
);

const AllTeachersPage = lazy(() => import("./screens/Teachers/AllTeachers/AllTeachers"));
const EditTeacherPage = lazy(() => import("./screens/Teachers/EditTeacher/EditTeacher"));

const AddCourse = lazy(() => import("./screens/Courses/AddCourse/AddCourse"));
const AllCourses = lazy(() => import("./screens/Courses/AllCourses/AllCourses"));
const EditCourses = lazy(() => import("./screens/Courses/EditCourses/EditCourses"));

const AllTerms = lazy(() => import("./screens/Terms/AllTerms/AllTerms"));
const AddTerm = lazy(() => import("./screens/Terms/AddTerm/AddTerm"));
const EditTerms = lazy(() => import("./screens/Terms/EditTerms/EditTerms"));
const AddStudentToTerm = lazy(() => import("./screens/Terms/AddStudentToTerm/AddStudentToTerm"));

const AllNews = lazy(() => import("./screens/News/AllNews/AllNews"));
const AllArticles = lazy(() => import("./screens/News/AllArticles/AllArticles"));
const AddNews = lazy(() => import("./screens/News/AddNews/AddNews"));
const EditNewsArticle = lazy(() => import("./screens/News/EditNewsArticle/EditNewsArticle"));

const AllComments = lazy(() => import("./screens/Comments/AllComments/AllComments"));


// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => {
      return (
        <ContextLayout.Consumer>
          {(context) => {
            return (
              <VerticalLayout {...props} permission="admin">
                <Suspense fallback={<Spinner />}>
                  <Component {...props} />
                </Suspense>
              </VerticalLayout>
            );
          }}
        </ContextLayout.Consumer>
      );
    }}
  />
  );
  // const mapStateToProps = state => {
    //   return {
      //     user: state.auth.login.userRole
      //   }
      // }
      
      const AppRoute = RouteConfig;

      
class AppRouter extends React.Component {
  state = {employee:getCurrentEmployee()}
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <EmployeeProvider value={this.state.employee}>
          <AppRoute exact path="/" component={Dashboard} />
          <AppRoute path="/page2" component={Page2} />
          <AppRoute path="/pages/login" component={login} fullLayout />
          <AppRoute path="/dashboard" component={Dashboard} />

        <AppRoute path="/allstudents" component={AllStudentsPage} />
          <AppRoute path="/editStudent/:id" component={EditStudents} />
          <AppRoute path="/deleteStudent/:id" component={DeleteStudents} />
          <AppRoute path="/activeStudent/:id" exact component={ActiveStudent} />
          <AppRoute
            path="/deactiveStudent/:id"
            exact
            component={DeactiveStudent}
          />

          <AppRoute path="/allTeachers" exact component={AllTeachersPage} />
          <AppRoute
            path="/allTeachers/editTeacher/:id"
            component={EditTeacherPage}
            exact
          />
          <AppRoute path="/addCourse" exact component={AddCourse} />
          <AppRoute path="/allCourses" exact component={AllCourses} />
          <AppRoute path="/editCourse/:id" exact component={EditCourses} />

          <AppRoute path="/allTerms" component={AllTerms} />
          <AppRoute path="/addTerm" component={AddTerm} />    
          <AppRoute path="/editTerm/:id" component={EditTerms} />
          <AppRoute path="/addStudentToTerm" component={AddStudentToTerm} />

          <AppRoute path="/allNews" component={AllNews} />
          <AppRoute path="/allArticles" component={AllArticles} />
          <AppRoute path="/addNewsArticle" component={AddNews} />
          <AppRoute path="/editNewsArticle/:id" component={EditNewsArticle} />

          <AppRoute path="/allComments" component={AllComments} />
          </EmployeeProvider>
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
