const news = {
  allNews: `${process.env.REACT_APP_Host}/api/news`,
  newsPagination: `${process.env.REACT_APP_Host}/api/news/list`,
  _commentNewsPagination:
    "queryString: ?pagenumber=1&pagesize=4&category='article'",
  topNews: `${process.env.REACT_APP_Host}/api/news/topNews`,
  newsById: `${process.env.REACT_APP_Host}/api/news/`,
  _commentNewsById: "after slash = :newsId",

  category: `${process.env.REACT_APP_Host}/api/news/category/`,
  _commentCategory: "after slash = :category",
  addNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  updateNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentUpdateNewsArticle: "after slash = :newsId",
  deleteNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentDeleteNewsArticle: "after slash = :newsId",
};

const article = {
  topArticle: `${process.env.REACT_APP_Host}/api/topArticles`,
};

const uploadPicture = {
  upload: `${process.env.REACT_APP_Host}/api/upload/image`,
};

const newsArticle = {
  category: `${process.env.REACT_APP_Host}/api/news/category/`,
  _commentCategory: "after slash = :category",
  addNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  updateNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentUpdateNewsArticle: "after slash = :newsId",
  deleteNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentDeleteNewsArticle: "after slash = :newsId",
};

const course = {
  getAllCourses: `${process.env.REACT_APP_Host}/api/course`,
  getCourseById: `${process.env.REACT_APP_Host}/api/course/`,
  _commentGetCourseById: "after slash = :courseId",
  coursePagination: `${process.env.REACT_APP_Host}/api/course/list`,
  _commentCoursePagination: "queryString: ?pagenumber=1&pagesize=4",
  getCourseForTerm: `${process.env.REACT_APP_Host}/api/course/term/`,
  _commentGetCourseForTerm: "after slash = :termId",
  addCourse: `${process.env.REACT_APP_Host}/api/course/add`,
  updateCourse: `${process.env.REACT_APP_Host}/api/course/`,
  _commentUpdateCourse: "after slash = :courseId",
  deleteCourse: `${process.env.REACT_APP_Host}/api/course/`,
  _commentDeleteCourse: "after slash = :courseId",
};
const term = {
  getAllTerms: `${process.env.REACT_APP_Host}/api/term/getall`,
  getTermById: `${process.env.REACT_APP_Host}/api/term/`,
  getTermForPagination: `${process.env.REACT_APP_Host}/api/term/list`,
  createTerm: `${process.env.REACT_APP_Host}/api/term`,
  updateTerm: `${process.env.REACT_APP_Host}/api/term/`,
  deleteTerm: `${process.env.REACT_APP_Host}/api/term/`,
  addStudentToTerm: `${process.env.REACT_APP_Host}/api/term/addStudentToTerm/`,
  removeStudentFromTerm: `${process.env.REACT_APP_Host}/api/term/removeStudentFromTerm/`,
  likeTerm: `${process.env.REACT_APP_Host}/api/term/like`,
  dislikeTerm: `${process.env.REACT_APP_Host}/api/term/dislike`,
  countLikeTerm: `${process.env.REACT_APP_Host}/api/term/likeCount/`,
};
const regLog = {
  registerMember: `${process.env.REACT_APP_Host}/api/auth/register`,
  loginMember: `${process.env.REACT_APP_Host}/api/auth/login`,
  registerEmployee: `${process.env.REACT_APP_Host}/api/auth/employee/register`,
  loginEmpployee: `${process.env.REACT_APP_Host}/api/auth/employee/login`,
};

const password = {
  forgetPassword: `${process.env.REACT_APP_Host}/api/forgetpassword`,
  resetPassword: `${process.env.REACT_APP_Host}/api/resetPassword`,
  _commentResetPassword: "after slash :token",
};

const students = {
  getAllStudents: `${process.env.REACT_APP_Host}/api/student/getall`,
  getStudentsForPagination: `${process.env.REACT_APP_Host}/api/student/list`,
  getStudentsById: `${process.env.REACT_APP_Host}/api/student/`,
  _commentGetStudentsById: "after slash :id",
  updateStudentInfo: `${process.env.REACT_APP_Host}/api/student/`,
  _commentUpdateStudentInfo: "after slash :id",
  deleteStudentById: `${process.env.REACT_APP_Host}/api/student/`,
  _commentDeleteStudentById: "after slash :id",
  activeStudent: `${process.env.REACT_APP_Host}/api/student/active/`,
  _commentActiveStudent: "after slash :id",
  deactiveStudent: `${process.env.REACT_APP_Host}/api/student/deactive/`,
  _commentDeactiveStudent: "after slash :id",
};

const employee = {
  getAllEmployee: `${process.env.REACT_APP_Host}/api/employee/getall`,
  getAllTeachers: `${process.env.REACT_APP_Host}/api/employee/getallteachers`,
  getEmployeesForPagination: `${process.env.REACT_APP_Host}/api/employee/list`,
  _commentGetEmployeesForPagination: "query string : ?pagenumber=1&pagesize=10",
  getTeachersForPagination: `${process.env.REACT_APP_Host}/api/employee/getTeachers/list`,
  _commentGetTeachersForPagination: "query string : ?pagenumber=1&pagesize=2",
  getEmployeeById: `${process.env.REACT_APP_Host}/api/employee/`,
  _commentGetEmployeeById: "after slash :id",
  updateEmployeeById: `${process.env.REACT_APP_Host}/api/employee/`,
  _commentUpdateEmployeeById: "after slash :id",
  activeEmployee: `${process.env.REACT_APP_Host}/api/employee/active/`,
  _commentActiveEmployee: " after slash :id",
  deactiveEmployee: `${process.env.REACT_APP_Host}/api/employee/deactive/`,
  _commentDeactiveEmployee: " after slash :id",
  deleteEmployeeById: `${process.env.REACT_APP_Host}/api/employee/`,
  _commentdeleteEmployeeById: "after slash :id",
};

const comments={
  getAllComments:`${process.env.REACT_APP_Host}/api/comment/`,
  createComment:`${process.env.REACT_APP_Host}/api/comment/send`,
  verifyComment:`${process.env.REACT_APP_Host}/api/comment/verify`,
  answerComment:`${process.env.REACT_APP_Host}/api/comment/answer`,
};

export {
  news,
  article,
  newsArticle,
  course,
  term,
  regLog,
  password,
  students,
  employee,
  uploadPicture,
  comments
};
