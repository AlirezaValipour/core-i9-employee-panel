import http from "./httpServices";
import { term } from "../api/apiUrl";

const createTerm = async (obj) => {
  try {
    const { data } = await http.post(term.createTerm, obj);
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const getTerms = async () => {
  try {
    const { data } = await http.get(term.getAllTerms);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};


const getTermsForPagination = async (pageNumber, pageSize) => {
  try {
    const { data } = await http.get(
      term.getTermForPagination +
        `?pagenumber=${pageNumber}&pagesize=${pageSize}`
    );
    return data.result;
  } catch (error) {
    return null;
  }
};

const deleteTerm = async (id) => {
  try {
    const { data } = await http.delete(`${term.deleteTerm}${id}`);
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
};

const updateTerms = async (id, obj) => {
  try {
    const { data } = await http.put(`${term.updateTerm}${id}`, obj);
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const getTermById = async (id) => {
  try {
    const { data } = await http.get(`${term.getTermById}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const addStudentToTerm = async (id, termId) => {
  try {
    const { data } = await http.post(`${term.addStudentToTerm}${id}`, {
      termId: termId,
    });
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const removeStudentFromTerm = async (id, termId) => {
  try {
    const { data } = await http.post(`${term.removeStudentFromTerm}${id}`, {
      termId: termId,
    });
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};
export {
  createTerm,
  getTerms,
  getTermsForPagination,
  deleteTerm,
  updateTerms,
  getTermById,
  addStudentToTerm,
  removeStudentFromTerm,
};
