import http from "./httpServices";
import { news } from "../api/apiUrl";

const createNewsArticle=async (obj) =>{
  try{
      const{ data } =await http.post(news.addNewsArticle,obj);
      console.log(data.result);
      return data.result;
  }
  catch(error){
    console.log(error);
  }
}

const getAllNews = async () => {
 try{
  const { data } = await http.get(news.allNews);
  console.log(data);
  return data.result;
 } 
 catch(error){
   console.log(error);
  }
};

const getInfoByCategory = async (category) => {
  try{
   const { data } = await http.get(`${news.category}${category}`);
   console.log(data);
   return data.result;
  } 
  catch(error){
    console.log(error);
   }
 };
const getNewsByPagination = async (pageNumber,pageSize,category) => {
  try{
    const { data } = await http.get(news.newsPagination+ `?pagenumber=${pageNumber}&pagesize=${pageSize}&category=${category}`);
    console.log(data);
    return data.result;
  }
  catch(error){
    console.log(error);
  }
};

const updateNewsArticle = async (id, obj) => {
  try {
    const { data } = await http.put(`${news.updateNewsArticle}${id}`, obj);
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const getNewsById = async (id) => {
  try {
    const { data } = await http.get(`${news.newsById}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const deleteNewsAndArticle = async (id) => {
  try {
    const { data } = await http.delete(`${news.deleteNewsArticle}${id}`);
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
};
export { createNewsArticle,getAllNews,getNewsByPagination,getInfoByCategory ,updateNewsArticle,getNewsById,deleteNewsAndArticle };
