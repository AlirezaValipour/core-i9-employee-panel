import React from "react";
import jwt_decode from "jwt-decode";

import Cookies from "js-cookie";
import { employee } from "../api/apiUrl";
import http from "./httpServices";

const jwt = Cookies.get("token");
const id = jwt_decode(jwt)._id;

const userInfo = async () => {
  const info = await http.get(`${employee.getEmployeeById}${id}`);
  const data = info.data.result;
  return data;
};

const getTeachersForPagination = async (page, pageSize) => {
  const info = await http.get(`${employee.getAllTeachers}`);
  let data = info.data.result.filter((teacher) => {
    if (teacher.salt) return teacher;
  });
  data = data.splice((page - 1) * pageSize, pageSize);
  return data;
};

const getAllTeachers = async () => {
  const info = await http.get(`${employee.getAllTeachers}`);
  return info;
};

const getEmployeeById = async (id) => {
  const info = await http.get(`${employee.getEmployeeById}${id}`);
  console.log(info);
  return info;
};

const updateEmployeeById = async (id, obj) => {
  const info = await http.put(`${employee.updateEmployeeById}${id}`, obj);
  return info;
};

const deleteEmployeeById = async (id) => {
  const info = await http.delete(`${employee.deleteEmployeeById}${id}`);
  return info;
};

const activeEmployee = async (id) => {
  const info = await http.put(`${employee.activeEmployee}${id}`);
  console.log(info);
  return info;
};

const deactiveEmployee = async (id) => {
  const info = await http.put(`${employee.deactiveEmployee}${id}`);
  console.log(info);
  return info;
};

export {
  userInfo,
  getTeachersForPagination,
  getAllTeachers,
  getEmployeeById,
  updateEmployeeById,
  deleteEmployeeById,
  deactiveEmployee,
  activeEmployee,
};
