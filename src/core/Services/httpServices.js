import axios from "axios";
import { toast } from "react-toastify";
import Cookies from "js-cookie";
import jwtDecode from "jwt-decode";


import { uploadPicture } from "../api/apiUrl";

const tokenKey = "token";

axios.interceptors.response.use(null, (error) => {
  console.log("error", error.response);
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  // console.log("interceptor " + expectedError);
  if (!expectedError) {
    toast.error("خطایی رخ داده است. لطفا دوباره تلاش کنید.");
  } else {
    toast.error(error.response.data.message[0].message);
  }

  return Promise.reject(error);
});

setJwt(getJwt());

export function getJwt() {
  return Cookies.get(tokenKey);
  // return localStorage.getItem(tokenKey);
}

function setJwt(jwt) {
  axios.defaults.headers.common["x-auth-token"] = jwt;
}

export function getCurrentEmployee() {
  try {
    const jwt = Cookies.get(tokenKey);
    return jwtDecode(jwt);
  } catch (error) {
    return null;
  }
}

const uploadFile = async (formData) => {
  try {
    console.log(formData);
    const result = await axios.post(uploadPicture.upload, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    return result;
  } catch (error) {
    console.log(error);
  }
};

export default {
  get: axios.get,
  put: axios.put,
  post: axios.post,
  delete: axios.delete,
  setJwt,
  uploadFile,
  getCurrentEmployee
};
