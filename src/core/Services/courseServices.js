import http from "./httpServices";
import { course } from "../api/apiUrl";
import { toast } from "react-toastify";

const createCourse = async (obj) => {
  try {
    const { data } = await http.post(course.addCourse, obj);
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const getAllCourses = async () => {
  try {
    const { data } = await http.get(course.getAllCourses);
    console.log(data);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const getCoursesForPagination = async (pageNumber, pageSize) => {
  try {
    const { data } = await http.get(
      course.coursePagination + `?pagenumber=${pageNumber}&pagesize=${pageSize}`
    );
    console.log(data);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const updateCourses = async (id, obj) => {
  try {
    const { data } = await http.put(`${course.updateCourse}${id}`, obj);
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const getCourseById = async (id) => {
  try {
    const { data } = await http.get(`${course.getCourseById}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const deleteCourse = async (id) => {
  try {
    const { data } = await http.delete(`${course.deleteCourse}${id}`);
    return data;
  } catch (error) {
    toast.error(error);
  }
};

const getCourseForTermById = async (id) => {
  try {
    const { data } = await http.get(`${course.getCourseForTerm}${id}`);
    console.log(data.result);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

export {
  createCourse,
  getAllCourses,
  getCoursesForPagination,
  updateCourses,
  getCourseById,
  getCourseForTermById,
  deleteCourse,
};
