import http from "./httpServices";
import { comments } from "../api/apiUrl";

const createComment=async (obj) =>{
    try{
        const{ data } =await http.post(comments.createComment,obj);
        console.log(data.result);
        return data.result;
    }
    catch(error){
      console.log(error);
    }
  }

  const getAllComments = async () => {
    try{
     const {data} = await http.get(comments.getAllComments);
     console.log(data);
     return data;
    } 
    catch(error){
      console.log(error);
     }
   };

   const verifyComment= async (obj) => {
       try{
        const {data} = await http.post(comments.verifyComment,obj);
        console.log(data);
         return data;
       }
       catch(error){
           console.log(error);
       }
   }

   const answerComment= async (obj) => {
    try{
     const { data } = await http.post(comments.answerComment,obj);
     console.log(data);
     return data.result;
    }
    catch(error){
        console.log(error);
    }
}
  export{createComment,getAllComments,verifyComment,answerComment};