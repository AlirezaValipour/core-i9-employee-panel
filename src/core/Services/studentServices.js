import http from "./httpServices";
import { students } from "../api/apiUrl";

const getStudents = async () => {
  try {
    const { data } = await http.get(students.getAllStudents);
    return data.result;
  } catch (error) {
    return null;
  }
};

const getStudentsForPagination = async (pageNumber, pageSize) => {
  try {
    const { data } = await http.get(
      students.getStudentsForPagination +
        `?pagenumber=${pageNumber}&pagesize=${pageSize}`
    );
    return data.result.students;
  } catch (error) {
    return null;
  }
};

const getStudentById = async (id) => {
  try {
    const { data } = await http.get(`${students.getStudentsById}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const updateStudent = async (id, obj) => {
  try {
    const { data } = await http.put(`${students.updateStudentInfo}${id}`, obj);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};
const deleteStudentById = async (id) => {
  try {
    const { data } = await http.delete(`${students.deleteStudentById}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const activeStudent = async (id) => {
  try {
    const { data } = await http.put(`${students.activeStudent}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};

const deactiveStudent = async (id) => {
  try {
    const { data } = await http.put(`${students.deactiveStudent}${id}`);
    return data.result;
  } catch (error) {
    console.log(error);
  }
};
export {
  getStudents,
  getStudentsForPagination,
  getStudentById,
  deleteStudentById,
  updateStudent,
  activeStudent,
  deactiveStudent,
};
