import React from 'react';

const Employee = React.createContext({});

export const EmployeeProvider = Employee.Provider;

export default Employee;
