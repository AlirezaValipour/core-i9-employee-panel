import Jalali from "jalali-moment";
export const convertToPersian = (
  miladiDate,
  inputFormat,
  outputFormat,
  miladiTime = `${new Date().getUTCHours()}:${new Date().getUTCMinutes()}`
) => {
  // convert to persian date
  let date = miladiDate;
  let convertedDate = {
    date: Jalali(date, inputFormat).locale("fa").format(outputFormat),
    time: null,
    persianDate: null,
  };
  const offset = new Date().getTimezoneOffset();
  let time = miladiTime.split(":");
  miladiTime = parseInt(time[0] * 60) + parseInt(time[1]);
  let shamsiTime = miladiTime + -offset;
  let shamsiHour = Math.floor(shamsiTime / 60);
  let shamsiMinute = shamsiTime - shamsiHour * 60;
  if (shamsiHour > 24) {
    convertedDate.date = Jalali(date, inputFormat)
      .add(1, "day")
      .locale("fa")
      .format(outputFormat);
    shamsiHour -= 24;
  }
  convertedDate.time =
    shamsiMinute > 0 ? `${shamsiHour}:${shamsiMinute}` : `${shamsiHour}`;

  let d = convertedDate.date.split("-");
  let mon;
  switch (d[1]) {
    case "01":
      mon = "فروردین";
      break;
    case "1":
      mon = "فروردین";
      break;
    case "02":
      mon = "اردیبهشت";
      break;
    case "2":
      mon = "اردیبهشت";
      break;
    case "03":
      mon = "خرداد";
      break;
    case "3":
      mon = "خرداد";
      break;
    case "04":
      mon = "تیر";
      break;
    case "4":
      mon = "تیر";
      break;
    case "05":
      mon = "مرداد";
      break;
    case "5":
      mon = "مرداد";
      break;
    case "06":
      mon = "شهریور";
      break;
    case "6":
      mon = "شهریور";
      break;
    case "07":
      mon = "مهر";
      break;
    case "7":
      mon = "مهر";
      break;
    case "08":
      mon = "آبان";
      break;
    case "8":
      mon = "آبان";
      break;
    case "09":
      mon = "آذر";
      break;
    case "9":
      mon = "آذر";
      break;
    case "10":
      mon = "دی";
      break;
    case "11":
      mon = "بهمن";
      break;
    case "12":
      mon = "اسفند";
      break;
  }
  convertedDate.persianDate = `${d[2]} ${mon} ${d[0]}`;

  return convertedDate;
};

export const dateDifference = (date1, date2) => {
  let d1 = date1.split("-");
  let d2 = date2.split("-");
  let days =
    (parseInt(d2[0]) - parseInt(d1[0])) * 365 +
    (parseInt(d2[1]) - parseInt(d1[1])) * 30 +
    (parseInt(d2[2]) - parseInt(d1[2]));

  return days;
};

export const dateDifference2 = (date1, date2) => {
  let d1 = date1.split("/");
  let d2 = date2.split("/");
  let days =
    (parseInt(d2[0]) - parseInt(d1[0])) * 365 +
    (parseInt(d2[1]) - parseInt(d1[1])) * 30 +
    (parseInt(d2[2]) - parseInt(d1[2]));

  return days;
};

export const timeStatus = (start, end) => {
  const now = Jalali().format("YYYY-MM-DD");
  let situation = {
    fromStart: dateDifference(now, start),
    fromEnd: dateDifference(now, end),
    stat: null,
  };
  if (situation.fromStart > 0) {
    situation.stat = "شروع نشده";
  } else if (situation.fromStart < 0 && situation.fromEnd > 0) {
    situation.stat = "در حال برگزاری";
  } else if (situation.fromStart === 0) {
    situation.stat = "شروع از امروز";
  } else {
    situation.stat = "پایان یافته";
  }
  return situation;
};

export const normalizeDateTime = (dateTime) => {
  console.log(dateTime);
  let dateAndTime = dateTime.split("T");
  let date = convertToPersian(dateAndTime[0], "YYYY-MM-DD", "YYYY-MM-DD");
  console.log(date);
  return date;
};

export const normalizeDateTimePersian = (dateTime) => {
  console.log(dateTime);
  let dateAndTime = dateTime.split("T");
  let date = dateAndTime[0].substr(0, 10);
  let dateArray = date.split("/");
  let mon;
  console.log(dateArray[1]);
  switch (dateArray[1]) {
    case "01":
      mon = "فروردین";
      break;
    case "1":
      mon = "فروردین";
      break;
    case "02":
      mon = "اردیبهشت";
      break;
    case "2":
      mon = "اردیبهشت";
      break;
    case "03":
      mon = "خرداد";
      break;
    case "3":
      mon = "خرداد";
      break;
    case "04":
      mon = "تیر";
      break;
    case "4":
      mon = "تیر";
      break;
    case "05":
      mon = "مرداد";
      break;
    case "5":
      mon = "مرداد";
      break;
    case "06":
      mon = "شهریور";
      break;
    case "6":
      mon = "شهریور";
      break;
    case "07":
      mon = "مهر";
      break;
    case "7":
      mon = "مهر";
      break;
    case "08":
      mon = "آبان";
      break;
    case "8":
      mon = "آبان";
      break;
    case "09":
      mon = "آذر";
      break;
    case "9":
      mon = "آذر";
      break;
    case "10":
      mon = "دی";
      break;
    case "11":
      mon = "بهمن";
      break;
    case "12":
      mon = "اسفند";
      break;
  }
  console.log(mon);
  const data = `${dateArray[2]} ${mon} ${dateArray[0]}`;
  return data;
};


export const normalizeDateTimePersian2 = (dateTime) => {
  console.log(dateTime);
  let dateAndTime = dateTime.split("T");
  let date = dateAndTime[0].substr(0, 10);
  let dateArray = date.split("-");
  let mon;
  console.log(dateArray[1]);
  switch (dateArray[1]) {
    case "01":
      mon = "فروردین";
      break;
    case "1":
      mon = "فروردین";
      break;
    case "02":
      mon = "اردیبهشت";
      break;
    case "2":
      mon = "اردیبهشت";
      break;
    case "03":
      mon = "خرداد";
      break;
    case "3":
      mon = "خرداد";
      break;
    case "04":
      mon = "تیر";
      break;
    case "4":
      mon = "تیر";
      break;
    case "05":
      mon = "مرداد";
      break;
    case "5":
      mon = "مرداد";
      break;
    case "06":
      mon = "شهریور";
      break;
    case "6":
      mon = "شهریور";
      break;
    case "07":
      mon = "مهر";
      break;
    case "7":
      mon = "مهر";
      break;
    case "08":
      mon = "آبان";
      break;
    case "8":
      mon = "آبان";
      break;
    case "09":
      mon = "آذر";
      break;
    case "9":
      mon = "آذر";
      break;
    case "10":
      mon = "دی";
      break;
    case "11":
      mon = "بهمن";
      break;
    case "12":
      mon = "اسفند";
      break;
  }
  console.log(mon);
  const data = `${dateArray[2]} ${mon} ${dateArray[0]}`;
  return data;
};

