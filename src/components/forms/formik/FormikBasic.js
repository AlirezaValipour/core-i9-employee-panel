import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  FormGroup
} from "reactstrap"
import { Formik, Field, Form } from "formik"
import { ToastContainer, toast } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"

class FormikBasic extends React.Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle><strong className="text-primary">ویرایش دانشجو</strong></CardTitle>
        </CardHeader>
        <CardBody>
          <Formik
            initialValues={{
              fullName: "",
              nationalId: "",
              email: "",
              phoneNumber: "",
              birthDate: "",
            }}
            onSubmit={values => {
              setTimeout(() => {
                toast.success(JSON.stringify(values, null, 2))
              }, 500)
            }}
            render={() => (
              <Form>
                <FormGroup>
                  <label htmlFor="fullName">نام ونام خانوادگی</label>
                  <Field
                    className="form-control"
                    name="fullName"
                    placeholder=""
                    type="text"

                  />
                </FormGroup>

                <FormGroup>
                  <label htmlFor="nationalId">کد ملی</label>
                  <Field
                    className="form-control"
                    name="nationalId"
                    placeholder=""
                    type="text"

                  />
                </FormGroup>
                <FormGroup>
                  <label htmlFor="email">ایمیل</label>
                  <Field
                    className="form-control"
                    name="email"
                    placeholder=""
                    type="email"
                  />
                </FormGroup>

                <FormGroup>
                  <label htmlFor="phoneNumber">شماره همراه</label>
                  <Field
                    className="form-control"
                    name="phoneNumber"
                    placeholder=""
                    type="text"
                  />
                </FormGroup>

                <FormGroup>
                  <label htmlFor="birthDate">تاریخ تولد</label>
                  <Field
                    className="form-control"
                    name="birthDate"
                    placeholder=""
                    type="text"
                  />
                </FormGroup>
                <Button.Ripple color="primary" type="submit">
                  ثبت تغییرات
                </Button.Ripple>
              </Form>
            )}
          />
          <ToastContainer />
        </CardBody>
      </Card>
    )
  }
}
export default FormikBasic
