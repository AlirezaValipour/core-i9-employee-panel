import React, { useState, useEffect, useContext } from "react";
import { Book } from "react-feather";
import { revenueGeneratedSeries, revenueGenerated } from "../StatisticsData";
import StatisticsCards from "../../@vuexy/statisticsCard/StatisticsCard";
import { getAllCourses } from "../../../core/Services/courseServices";
import { dateDifference } from "../../../core/utils/DateHandling";
import Employee from "../../../core/Context/context";

const CoursesAdded = () => {
  const [courses, setCourses] = useState([]);
  const employee = useContext(Employee);

  useEffect(() => {
    async function getCounts() {
      if (employee.role === "admin") {
        const courses = await getAllCourses();
        setCourses(courses);
      }
    }
    getCounts();
  }, []);

  const loadData = () => {
    if (courses) {
      const today = new Date();
      const todayDate =
        today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();

      const dataNumbers = [0, 0, 0, 0];
      for (let i = 0; i < courses.length; i++) {
        let date = dateDifference(
          todayDate,
          courses[i].createDate.substr(0, 10)
        );
        if (date >= 0 && date < 7) {
          dataNumbers[3]++;
        } else if (date >= 7 && date < 14) {
          dataNumbers[2]++;
        } else if (date >= 14 && date < 21) {
          dataNumbers[1]++;
        } else if (date >= 21 && date < 28) {
          dataNumbers[0]++;
        }
      }
      return dataNumbers;
    }
  };
  return (
    <>
      {employee.role === "admin" && (
        <StatisticsCards
          icon={<Book className="success" size={22} />}
          iconBg="success"
          stat={`${courses.length} دوره`}
          statTitle="تعداد دوره ها"
          options={revenueGenerated}
          series={[
            {
              name: "دوره",
              data: loadData(),
            },
          ]}
          type="area"
        />
      )}
    </>
  );
};

export default CoursesAdded;
