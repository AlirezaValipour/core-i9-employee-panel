import React, { useState, useEffect, useContext } from "react";
import { Users } from "react-feather";
import { ordersReceived, ordersReceivedSeries } from "../StatisticsData";
import StatisticsCards from "../../@vuexy/statisticsCard/StatisticsCard";
import { dateDifference } from "../../../core/utils/DateHandling";
import { getAllTeachers } from "../../../core/Services/employeeService";
import Employee from "../../../core/Context/context";

const MastersChart = () => {
  const [teachers, setTeachers] = useState([]);
  const employee = useContext(Employee);
  console.log(employee);
  useEffect(() => {
    async function getTeachers() {
      if (employee.role === "admin") {
        const data = await getAllTeachers();
        console.log(data.data.result);
        setTeachers(data.data.result);
      }
    }
    getTeachers();
  }, []);

  const loadData = () => {
    if (teachers) {
      const today = new Date();
      const todayDate =
        today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();

      const dataNumbers = [0, 0, 0, 0];
      for (let i = 0; i < teachers.length; i++) {
        let date = dateDifference(
          todayDate,
          teachers[i].registerDate.substr(0, 10)
        );
        if (date >= 0 && date < 7) {
          dataNumbers[3]++;
        } else if (date >= 7 && date < 14) {
          dataNumbers[2]++;
        } else if (date >= 14 && date < 21) {
          dataNumbers[1]++;
        } else if (date >= 21 && date < 28) {
          dataNumbers[0]++;
        }
      }
      return dataNumbers;
    }
  };
  return (
    <>
      {employee.role === "admin" && (
        <StatisticsCards
          icon={<Users className="warning" size={22} />}
          iconBg="warning"
          stat={`${teachers.length} نفر`}
          statTitle="تعداد اساتید"
          options={ordersReceived}
          series={[
            {
              name: "استاد",
              data: loadData(),
            },
          ]}
          type="area"
        />
      )}
    </>
  );
};

export default MastersChart;
