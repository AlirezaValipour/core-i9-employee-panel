import React, { useState, useEffect, useContext } from "react";
import { Users } from "react-feather";
import { subscribersGained } from "../StatisticsData";
import StatisticsCards from "../../@vuexy/statisticsCard/StatisticsCard";
import { getStudents } from "../../../core/Services/studentServices";
import { dateDifference } from "../../../core/utils/DateHandling";
import Employee from "../../../core/Context/context";

const StudentGained = () => {
  const [students, setStudents] = useState([]);
  const [studentsData, setStudentsData] = useState();
  const employee = useContext(Employee);
  console.log(employee);
  useEffect(() => {
    async function getCounts() {
      if (employee.role === "admin") {
        const array = await getStudents();
        setStudentsData(array);
        setStudents(array.length);
      }
    }
    getCounts();
  }, []);

  const loadData = () => {
    if (studentsData) {
      const today = new Date();
      const todayDate =
        today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();

      const dataNumbers = [0, 0, 0, 0];
      // console.log(students);
      for (let i = 0; i < students; i++) {
        let date = dateDifference(
          todayDate,
          studentsData[i].registerDate.substr(0, 10)
        );
        if (date >= 0 && date < 7) {
          dataNumbers[3]++;
        } else if (date >= 7 && date < 14) {
          dataNumbers[2]++;
        } else if (date >= 14 && date < 21) {
          dataNumbers[1]++;
        } else if (date >= 21 && date < 28) {
          dataNumbers[0]++;
        }
        // console.log(dataNumbers);
      }
      return dataNumbers;
    }
  };
  return (
    <>
      {employee.role === "admin" && students && (
        <StatisticsCards
          icon={<Users className="primary" size={22} />}
          stat={`${students} نفر`}
          statTitle="تعداد دانشجویان"
          options={subscribersGained}
          series={[
            {
              name: "دانشجو",
              data: loadData(),
            },
          ]}
          type="area"
        />
      )}
    </>
  );
};

export default StudentGained;
