import React from "react";
import { Row, Col } from "reactstrap";
import StudentGained from "./StudentGained/StudentGained";
import CoursesAdded from "./CoursesAdded/CoursesAdded";
import MastersChart from "./MastersChart/MastersChart";

const DashboardHeader = () => {
  return (
    <>
      <Row className="match-height">
        <Col lg="4" md="6" sm="12">
          <StudentGained />
        </Col>
        <Col lg="4" md="6" sm="12">
          <CoursesAdded />
        </Col>
        <Col lg="4" md="12" sm="12">
          <MastersChart />
        </Col>
      </Row>
    </>
  );
};

export default DashboardHeader;
