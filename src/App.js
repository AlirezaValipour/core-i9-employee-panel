import React,{useContext}from "react";
import Router from "./Router";

import "./components/@vuexy/rippleButton/RippleButton";

import "react-perfect-scrollbar/dist/css/styles.css";
import "prismjs/themes/prism-tomorrow.css";

import Employee from "./core/Context/context";

const App = (props) => {
  const employee = useContext(Employee);

  return <Router employee={employee} />;
};

export default App;
