import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
} from "reactstrap";
import DataTable from "react-data-table-component";
import * as Icon from "react-feather";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getAllComments,
  verifyComment,
  answerComment,
} from "../../../core/Services/commentService";
import Loading from "../../../components/common/Loading/Loading";
import { WordSeperator } from "../../../core/utils/StringManipulation";
import { normalizeDateTime } from "../../../core/utils/DateHandling";

const AllComments = () => {
  const [comments, setComments] = useState();
  const [id, setId] = useState();
  const [filteredData, setFilteredData] = useState([]);
  const [value, setValue] = useState("");
  const [modal, setModal] = useState(false);
  const [update, setUpdate] = useState(false);
  const [columns, setState] = useState([
    {
      name: "متن کامنت",
      selector: "comment",
      width: "44%",
      fontSize: "1rem",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => <div>{WordSeperator(row.comment, 20)}</div>,
    },

    {
      name: "تاریخ ثبت",
      selector: "createDate",
      sortable: true,
      width: "26%",
      center: true,
      style: { fontSize: "1.1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTime(row.createDate).persianDate}
        </p>
      ),
    },

    {
      name: "وضعیت کامنت",
      selector: "verified",
      sortable: true,
      width: "25%",
      center: true,
      style: { fontSize: "1.3rem" },
      cell: (row) =>
        row.verified ? (
          <div className="badge badge-success">تایید شده</div>
        ) : (
          <div
            className="badge badge-danger"
            style={{ cursor: "pointer" }}
            onClick={() => {
              toggle();
              setId(row._id);
            }}
          >
            تایید نشده
          </div>
        ),
    },
  ]);

  useEffect(() => {
    async function setStates() {
      const res = await getAllComments();
      setComments(res.reverse());
      console.log(res.reverse());
    }
    setStates();
  }, [update]);

  const toggle = () => setModal(!modal);

  const handleFilter = (e) => {
    let value = e.target.value;
    let data = comments;
    let filteredDataa = filteredData;

    if (value.length) {
      filteredDataa = data.filter((item) => {
        let startsWithCondition = item.comment
          .toLowerCase()
          .startsWith(value.toLowerCase());

        let includesCondition = item.comment
          .toLowerCase()
          .includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      setFilteredData(filteredDataa);
    }
    setValue(value);
  };

  const ExpandableRow = ({ data }) => {
    return (
      <React.Fragment>
        <div className="my-1">
          <div className="text-danger">متن کامنت : </div>
          <div className="mb-1">{data.comment}</div>
        </div>
        <Formik
          initialValues={{
            answer: "",
          }}
          enableReinitialize={true}
          onSubmit={async (values, { resetForm }) => {
            console.log(values);
            await answerComment({ id: data._id, answer: values.answer });
            toast.success("کامنت با موفقیت ارسال شد");
            resetForm();
            setUpdate(!update);
          }}
        >
          <Form>
            <FormGroup>
              <Label htmlFor="answer" className="text-primary">
                <strong> پاسخ به کامنت</strong>
              </Label>
              <Field
                as="textarea"
                name="answer"
                className="form-control"
              ></Field>
            </FormGroup>
            <Button.Ripple className="mb-1" color="primary" type="submit">
              ارسال
            </Button.Ripple>
          </Form>
        </Formik>
      </React.Fragment>
    );
  };

  return (
    <>
      {comments ? (
        <>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">لیست کامنت ها</strong>
              </CardTitle>
            </CardHeader>
            <CardBody className="rdt_Wrapper">
              <DataTable
                className="dataTable-custom"
                highlightOnHover={true}
                style={{ fontSize: "1rem" }}
                data={value.length ? filteredData : comments}
                columns={columns}
                noHeader
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                  <div className="d-flex flex-wrap justify-content-between">
                    <div className="position-relative has-icon-left mb-1">
                      <Input
                        value={value}
                        onChange={(e) => handleFilter(e)}
                        placeholder="جستجو ..."
                      />
                      <div className="form-control-position">
                        <Icon.Search size="15" />
                      </div>
                    </div>
                  </div>
                }
                customStyles={{
                  headCells: {
                    style: {
                      fontSize: "1.3rem",
                      textAlign: "center",
                    },
                    pagination: {
                      style: { textAlign: "center" },
                      pageButtonsStyle: {
                        border: "1px solid red",
                      },
                    },
                  },
                }}
                expandableRows
                expandOnRowClicked
                expandableRowsComponent={<ExpandableRow />}
                expandableRowExpanded={(row) => row._id.expandableRowExpanded}
              />
            </CardBody>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader>
                <strong>تایید کامنت</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به تایید این کامنت هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={async () => {
                    toggle();
                    await verifyComment({ id: id });
                    setUpdate(!update);
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
          </Card>
          <ToastContainer position="top-center" />
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default AllComments;
