import React, { useState, useEffect, useContext } from "react";
import "../../assets/scss/plugins/charts/apex-charts.scss";
import DashboardHeader from "./../../components/DashboardHeader/DashboardHeader";
import { Card, CardBody, CardHeader, CardTitle, Table } from "reactstrap";
import {
  getStudents,
  getStudentsForPagination,
} from "../../core/Services/studentServices";
import {
  getTerms,
  getTermsForPagination,
} from "../../core/Services/termServices";
import { normalizeDateTime } from "../../core/utils/DateHandling";
import Pagination from "../../components/common/Pagination/Pagination";
import Employee from "../../core/Context/context";

const Dashboard = () => {
  const [students, setStudents] = useState([]);
  const [terms, setTerms] = useState([]);
  const [count, setCount] = useState();
  const [page, setPage] = useState(1);
  const employee = useContext(Employee);
  useEffect(() => {
    async function setStates() {
      if (employee.role === "admin") {
        const students = await getStudentsForPagination(page, 10);
        const studentCount = await getStudents();
        setCount(studentCount.length);
        setStudents(students);
      } else {
        const { terms } = await getTermsForPagination(page, 10);
        console.log(terms);
        const termCount = await getTerms();
        setCount(termCount.length);
        setTerms(terms);
        console.log(terms);
      }
    }
    setStates();
  }, [page]);

  const handleStateChange = (pageNumber) => {
    setPage(pageNumber);
  };
  // terms.map((term) => console.log(term.students));

  return (
    <>
      <DashboardHeader />
      {employee.role === "admin" ? (
        <Card>
          <CardHeader>
            <CardTitle className="text-primary">
              <strong>لیست دانشجویان</strong>
            </CardTitle>
          </CardHeader>
          <CardBody className="text-center">
            <Table hover responsive bordered>
              <thead>
                <tr>
                  <th>نام و نام خانوادگی</th>
                  <th>ایمیل</th>
                  <th>تاریخ تولد</th>
                  <th>تاریخ ثبت نام</th>
                  <th>شماره همراه</th>
                </tr>
              </thead>
              <tbody>
                {students.map((student) => (
                  <tr key={student._id}>
                    <td key={student.fullName}>{student.fullName}</td>
                    <td key={student.email}>{student.email}</td>
                    <td key={student.birthDate}>{student.birthDate}</td>
                    <td key={student.registerDate}>
                      {normalizeDateTime(student.registerDate).persianDate}
                    </td>
                    <td key={student.phoneNumber}>{student.phoneNumber}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            {students && (
              <Pagination
                itemCount={count}
                pageSize={10}
                setActive={handleStateChange}
                active={page}
              />
            )}
          </CardBody>
        </Card>
      ) : (
        <Card dir="rtl">
          <CardHeader>
            <CardTitle className="text-primary">
              <strong>لیست ترم ها</strong>
            </CardTitle>
          </CardHeader>
          <CardBody className="text-center">
            <Table hover responsive bordered>
              <thead>
                <tr>
                  <th>عکس دوره</th>
                  <th>نام دوره</th>
                  <th>مدرس دوره</th>
                  <th>نام ترم</th>
                  <th>قیمت ترم</th>
                  <th>تعداد دانشجویان</th>
                </tr>
              </thead>
              <tbody>
                {terms.map((term) => (
                  <tr key={term._id}>
                    <td className="w-25" key={term.course.image}>
                      <img
                        className="w-75"
                        src={term.course.image}
                        alt="no pics"
                      />
                    </td>
                    <td key={term.course.courseName}>
                      {term.course.courseName}
                    </td>
                    <td key={term.teacher.fullName}>{term.teacher.fullName}</td>
                    <td key={term.title}>{term.title}</td>
                    <td key={term.cost}>{term.cost + " تومان"}</td>
                    <td key={term.students.map((student) => student.fullName)}>
                      {term.students.length !== 0
                        ? term.students.length + " نفر"
                        : "صفر"}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            {terms && (
              <Pagination
                itemCount={count}
                pageSize={10}
                setActive={handleStateChange}
                active={page}
              />
            )}
          </CardBody>
        </Card>
      )}
    </>
  );
};

export default Dashboard;
