import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  FormGroup,
  Label,
  FormText,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getCourseById,
  updateCourses,
} from "../../../core/Services/courseServices";
import http from "../../../core/Services/httpServices";
import Loading from "../../../components/common/Loading/Loading";
import * as Icon from "react-feather";

const EditCourses = ({ match }) => {
  const id = match.params.id;
  const [courseInfo, setCourseInfo] = useState({
    courseName: "",
    topics: ["topic"],
    description: "",
  });
  const [initialValues, setInitialValues] = useState({
    courseName: "",
    description: "",
  });
  const [image, setImage] = useState();
  const [topicCount, setTopicCount] = useState();
  const [update, setUpdate] = useState(false);
  useEffect(() => {
    async function getCourseInfo() {
      const info = await getCourseById(id);
      setCourseInfo(info);
      setImage(info.image);
      setTopicCount(info.topics.length);
      initialValuesObject(info);
    }
    getCourseInfo();
  }, []);

  const initialValuesObject = (info) => {
    let initialValuesObj = {
      courseName: info.courseName,
      description: info.description,
    };
    for (let i = 0; i < info.topics.length; i++) {
      initialValuesObj[`topic${i}`] = info.topics[i];
    }
    setInitialValues(initialValuesObj);
    console.log(initialValuesObj);
    return initialValuesObj;
  };

  const newTopics = (e) => {
    e.preventDefault();
    let topics = [...courseInfo.topics];
    topics.push("موضوع جدید");
    delete courseInfo.topics;
    let newCourseInfo = { ...courseInfo, topics };
    setCourseInfo(newCourseInfo);
    setTopicCount(topicCount + 1);
  };

  const submitHandler = async (values) => {
    let topics = [];
    for (let i = 0; i < topicCount; i++) {
      if (document.querySelector(`input[name=topic${i}]`)) {
        topics.push(values[`topic${i}`]);
        delete values[`topic${i}`];
      } else {
        delete values[`topic${i}`];
      }
    }
    console.log(topics);
    values.topics = topics;
    let data = { ...values };
    data.image = image;
    const res = await updateCourses(id, data);
    console.log(res);
    toast.success("تغییرات با موفقیت اعمال شد");
    console.log(values);
  };

  const imageUpload = async (e) => {
    const formData = new FormData();
    formData.append("image", e.target.files[0]);
    const res = await http.uploadFile(formData);
    toast.success("عکس مورد نظر آپلود شد.");
    console.log(res.data.result);
    setImage(res.data.result);
  };

  const deleteTopic = (index) => {
    const topic = document.querySelector(`ol > li:nth-child(${index + 1})`);
    topic.remove();
    setUpdate(!update);
    console.log(topic);
  };

  return (
    <>
      {courseInfo.courseName.length ? (
        <React.Fragment>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">ویرایش دوره</strong>
              </CardTitle>
            </CardHeader>
            <CardBody>
              <img
                src={image}
                className="w-75 mx-auto img-fluid d-block"
                alt="Course Picture"
              />
              <Formik
                initialValues={initialValues}
                enableReinitialize={true}
                onSubmit={(values) => {
                  submitHandler(values);
                }}
              >
                <Form>
                  <FormGroup>
                    <label htmlFor="courseName">نام دوره</label>
                    <Field
                      className="form-control"
                      name="courseName"
                      type="text"
                    />
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="topics">موضوعات دوره</label>
                    <ol>
                      {courseInfo.topics.map((item, index) => (
                        <li className="my-2" key={index}>
                          <Icon.X
                            className="d-inline-block"
                            color={"red"}
                            onClick={() => deleteTopic(index)}
                          ></Icon.X>
                          <Field
                            className="form-control d-inline-block w-75"
                            name={"topic" + index}
                            type="text"
                            placeholder="موضوع جدید"
                          />
                        </li>
                      ))}
                    </ol>
                    <Button.Ripple
                      color="primary"
                      type="submit"
                      onClick={(e) => newTopics(e)}
                    >
                      افزودن موضوع
                    </Button.Ripple>
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="description">شرح دوره</label>
                    <Field
                      as="textarea"
                      className="form-control"
                      type="textarea"
                      name="description"
                      style={{ minHeight: "100px" }}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label className="mr-1" htmlFor="image">
                      آپلود عکس
                    </label>

                    <Field
                      type="file"
                      name="image"
                      onChange={(e) => imageUpload(e)}
                    />
                    <FormText className="mt-1" color="danger">
                      برای تغییر عکس دوره، عکس مورد نظر را آپلود کنید.
                    </FormText>
                  </FormGroup>
                  <Button.Ripple color="primary" type="submit">
                    ثبت تغییرات
                  </Button.Ripple>
                </Form>
              </Formik>
              <ToastContainer position="top-center" />
            </CardBody>
          </Card>
        </React.Fragment>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default EditCourses;
