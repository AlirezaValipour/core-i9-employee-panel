import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Button,
  FormGroup,
  FormText,
  Label,
} from "reactstrap";
import * as Icon from "react-feather";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createCourse } from "../../../core/Services/courseServices";
import http from "../../../core/Services/httpServices";

const AddCourse = () => {
  let imageUrl;
  const [initialValues, setInitialValues] = useState({
    courseName: "",
    topics: ["موضوع جدید"],
    description: "",
    image: "",
  });
  const [topicCount, setTopicCount] = useState(1);
  const [update, setUpdate] = useState(false);

  const newTopics = (e) => {
    e.preventDefault();
    let topics = [...initialValues.topics];
    topics.push("موضوع جدید");
    delete initialValues.topics;
    let newCourseInfo = { ...initialValues, topics };
    setInitialValues(newCourseInfo);
    setTopicCount(topicCount + 1);
  };

  const submitHandlder = async (values) => {
    console.log(values);
    let topics = [];
    for (let i = 0; i < topicCount; i++) {
      if (document.querySelector(`input[name=topic${i}]`)) {
        topics.push(values[`topic${i}`]);
        delete values[`topic${i}`];
      } else {
        delete values[`topic${i}`];
      }
    }
    console.log(topics);
    values.topics = topics;
    let data = { ...values };
    data.image = imageUrl;
    console.log(data);
    const res = await createCourse(data);
    console.log(res);
    toast.success("تغییرات با موفقیت اعمال شد");
  };

  const deleteTopic = (index) => {
    const topic = document.querySelector(`ol > li:nth-child(${index + 1})`);
    topic.remove();
    setUpdate(!update);
    console.log(topic);
  };
  return (
    <React.Fragment>
      <Card>
        <CardHeader>
          <CardTitle>
            <strong className="text-primary">اضافه کردن دوره</strong>
          </CardTitle>
        </CardHeader>
        <CardBody>
          <Formik
            initialValues={{
              courseName: "",
              topics: [""],
              description: "",
              image: "",
            }}
            enableReinitialize={true}
            onSubmit={async (values, { resetForm }) => {
              submitHandlder(values);
              resetForm();
            }}
          >
            <Form>
              <FormGroup>
                <Label htmlFor="courseName">نام دوره</Label>

                <Field className="form-control" name="courseName" type="text" />
              </FormGroup>

              <FormGroup>
                <Label htmlFor="topics">موضوعات دوره</Label>
                {
                  <ol>
                    {initialValues.topics.map((item, index) => (
                      <li className="my-2" key={index}>
                        <Icon.X
                          className="d-inline-block"
                          color={"red"}
                          onClick={() => deleteTopic(index)}
                        ></Icon.X>
                        <Field
                          className="form-control d-inline-block w-75"
                          name={"topic" + index}
                          type="text"
                          placeholder="موضوع جدید"
                        />
                      </li>
                    ))}
                  </ol>
                }
                <Button.Ripple
                  color="primary"
                  type="submit"
                  onClick={(e) => newTopics(e)}
                >
                  افزودن موضوع
                </Button.Ripple>
              </FormGroup>
              <FormGroup>
                <Label htmlFor="description">شرح دوره</Label>
                <Field
                  as="textarea"
                  className="form-control"
                  type="textarea"
                  name="description"
                />
              </FormGroup>
              <FormGroup>
                <Label className="mr-1" for="image">
                  آپلود عکس
                </Label>

                <Field
                  onChange={async (e) => {
                    toast.info("لطفا منتظر آپلود شدن عکس باشید :)");
                    const formData = new FormData();
                    formData.append("image", e.target.files[0]);
                    const res = await http.uploadFile(formData);
                    imageUrl = res.data.result;
                    if (res.data.message[0].message)
                      toast.success("عکس مورد نظر آپلود شد :)");
                  }}
                  type="file"
                  name="image"
                />
                <FormText className="mt-1" color="danger">
                  عکس موردنظر خود را آپلود کنید
                </FormText>
              </FormGroup>
              <Button.Ripple color="primary" type="submit">
                ثبت تغییرات
              </Button.Ripple>
            </Form>
          </Formik>
          <ToastContainer position="top-center" />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

export default AddCourse;
