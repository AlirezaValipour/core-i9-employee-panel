import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import Loading from "../../../components/common/Loading/Loading";
import DataTable from "react-data-table-component";
import {
  getAllCourses,
  deleteCourse,
} from "../../../core/Services/courseServices";
import { Search } from "react-feather";
import { WordSeperator } from "../../../core/utils/StringManipulation";
import * as Icon from "react-feather";
import { normalizeDateTime } from "../../../core/utils/DateHandling";
const AllCourses = () => {
  const [cours, setCourses] = useState();
  const [modal, setModal] = useState(false);
  const [currentId, setCurrentId] = useState();
  const [filteredData, setFilteredData] = useState([]);
  const [value, setValue] = useState("");
  const [columns, setState] = useState([
    {
      name: "تصویر دوره",
      selector: "image",
      width: "15%",
      fontSize: "1rem",
      center: true,
      cell: (row) => (
        <div className="h-100 p-0">
          <img
            className="text-bold-500 mb-0 w-100"
            style={{ height: "150px", padding: "5px" }}
            src={row.image}
          />
        </div>
      ),
    },
    {
      name: "نام دوره",
      selector: "courseName",
      sortable: true,
      width: "15%",
      center: true,
      style: { fontSize: "1.1rem" },
      cell: (row) => (
        <div className="d-flex flex-xl-row flex-column align-items-xl-center align-items-start py-xl-0 py-1">
          {row.courseName}
        </div>
      ),
    },
    {
      name: "تاریخ ساخت",
      selector: "createDate",
      sortable: true,
      width: "18%",
      center: true,
      style: { fontSize: "1.1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTime(row.createDate).persianDate}
        </p>
      ),
    },
    {
      name: "توضیحات دوره",
      selector: "description",
      sortable: true,
      width: "18%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => <div>{WordSeperator(row.description, 20)}</div>,
    },
    {
      name: "تعداد عناوین",
      selector: "topics",
      sortable: true,
      width: "18%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.topics.length}</p>
      ),
    },
    {
      name: "ویرایش",
      selector: "edit",
      width: "8%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <Link to={`/editCourse/${row._id}`}>
          <Icon.Edit color={"blue"} size={26} />
        </Link>
      ),
    },
    {
      name: "حذف",
      selector: "delete",
      width: "8%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <span className=" .cursor-pointer" onClick={toggle}>
          <Icon.Trash2
            color={"red"}
            style={{ cursor: "pointer" }}
            size={26}
            onClick={() => {
              console.log(row._id);
              setCurrentId(row._id);
              toggle();
            }}
          ></Icon.Trash2>
        </span>
      ),
    },
  ]);

  useEffect(() => {
    async function setStates() {
      const courses = await getAllCourses();
      setCourses(courses.reverse());
      console.log(courses.reverse());
    }
    setStates();
  }, []);

  const toggle = () => setModal(!modal);

  const deleteCourseHandler = async (id) => {
    const data = await deleteCourse(id);
    console.log(data.result);
    setCourses(cours.filter((item) => item._id !== id));
  };

  const handleFilter = (e) => {
    let value = e.target.value;
    let data = cours;
    let filteredDataa = filteredData;

    if (value.length) {
      filteredDataa = data.filter((item) => {
        let startsWithCondition =
          item.courseName.toLowerCase().startsWith(value.toLowerCase()) ||
          item.description.toLowerCase().startsWith(value.toLowerCase()) ||
          item.createDate.toLowerCase().startsWith(value.toLowerCase());
        let includesCondition =
          item.courseName.toLowerCase().includes(value.toLowerCase()) ||
          item.description.toLowerCase().includes(value.toLowerCase()) ||
          item.createDate.toLowerCase().includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      setFilteredData(filteredDataa);
    }
    setValue(value);
  };

  return (
    <>
      {cours ? (
        <>
          <Row>
            <Col>
              <Link className="btn btn-primary mb-1" to="/addCourse">
                افزودن دوره
              </Link>
            </Col>
          </Row>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">لیست دوره ها</strong>
              </CardTitle>
            </CardHeader>
            <CardBody className="rdt_Wrapper">
              <DataTable
                className="dataTable-custom"
                highlightOnHover={true}
                style={{ fontSize: "2.3rem" }}
                data={value.length ? filteredData : cours}
                columns={columns}
                noHeader
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                  <div className="d-flex flex-wrap justify-content-between">
                    {/* <div className="add-new">
                      <Link to="/addCourse" style={{ color: "unset" }}>
                        <Button.Ripple color="primary">
                          افزودن دوره
                        </Button.Ripple>
                      </Link>
                    </div> */}
                    <div className="position-relative has-icon-left mb-1">
                      <Input
                        value={value}
                        onChange={(e) => handleFilter(e)}
                        placeholder="جستجو ..."
                      />
                      <div className="form-control-position">
                        <Search size="15" />
                      </div>
                    </div>
                  </div>
                }
                customStyles={{
                  headCells: {
                    style: {
                      fontSize: "1.3rem",
                      textAlign: "center",
                    },
                    pagination: {
                      style: { textAlign: "center" },
                      pageButtonsStyle: {
                        border: "1px solid red",
                      },
                    },
                  },
                }}
              />
            </CardBody>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader>
                <strong>حذف کاربر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به حذف این دوره هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={() => {
                    toggle();
                    deleteCourseHandler(currentId);
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
          </Card>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default AllCourses;
