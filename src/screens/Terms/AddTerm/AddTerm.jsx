import React, { useState, useEffect } from "react";
import {
  Col,
  Row,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Button,
  FormGroup,
  Label,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createTerm } from "../../../core/Services/termServices";
import { getAllCourses } from "../../../core/Services/courseServices";
import { getAllTeachers } from "../../../core/Services/employeeService";

const AddTerm = () => {
  const [teachers, setTeachers] = useState();
  const [courses, setCourses] = useState();

  useEffect(() => {
    async function getData() {
      const { data } = await getAllTeachers();
      const courses = await getAllCourses();
      console.log(data.result);

      setTeachers(data.result);
      setCourses(courses);
    }
    getData();
  }, []);
  console.log(teachers);
  console.log(courses);
  return (
    <React.Fragment>
      <Card>
        <CardHeader>
          <CardTitle>
            <strong className="text-primary">اضافه کردن ترم</strong>
          </CardTitle>
        </CardHeader>
        <CardBody>
          <Formik
            initialValues={{
              title: "",
              cost: "",
              endDate: "",
              startDate: "",
              capacity: "",
              teacher: "",
              course: "",
            }}
            enableReinitialize={true}
            onSubmit={async (values, { resetForm }) => {
              console.log(values);
              await createTerm(values);
              toast.success("تغییرات با موفقیت اعمال شد");
              resetForm();
            }}
          >
            <Form>
              <FormGroup>
                <Label htmlFor="title">نام ترم</Label>

                <Field className="form-control" name="title" type="text" />
              </FormGroup>

              <FormGroup>
                <Label htmlFor="cost">قیمت ترم</Label>
                <Field className="form-control" name="cost" type="text" />
              </FormGroup>
              <Row>
                <Col lg="6" md="6" sm="12">
                  <FormGroup>
                    <Label htmlFor="startDate">تاریخ شروع ترم</Label>
                    <Field
                      className="form-control text-left"
                      type="text"
                      name="startDate"
                    />
                  </FormGroup>
                </Col>
                <Col lg="6" md="6" sm="12">
                  <FormGroup>
                    <Label htmlFor="endDate">تاریخ پایان ترم</Label>
                    <Field
                      className="form-control text-left"
                      dir="rtl"
                      type="text"
                      name="endDate"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <FormGroup>
                <Label htmlFor="capacity">ظرفیت ترم</Label>
                <Field className="form-control" type="text" name="capacity" />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="teacher">مدرس ترم</Label>
                <Field as="select" name="teacher" className="form-control">
                  {teachers &&
                    teachers.map((teacher) => (
                      <option value={teacher._id}>{teacher.fullName}</option>
                    ))}
                </Field>
              </FormGroup>
              <FormGroup>
                <Label htmlFor="course">نام دوره ترم</Label>
                <Field as="select" name="course" className="form-control">
                  {courses &&
                    courses.map((course) => (
                      <option value={course._id}>{course.courseName}</option>
                    ))}
                </Field>
              </FormGroup>
              <Button.Ripple color="primary" type="submit">
                ثبت تغییرات
              </Button.Ripple>
            </Form>
          </Formik>
          <ToastContainer position="top-center" />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

export default AddTerm;
