import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Button,
  FormGroup,
  Label,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getStudents,
  getStudentById,
} from "../../../core/Services/studentServices";
import {
  addStudentToTerm,
  removeStudentFromTerm,
  getTerms,
} from "../../../core/Services/termServices";
import * as Icon from "react-feather";
import Loading from "./../../../components/common/Loading/Loading";

const AddStudentToTerm = () => {
  const [studentInfo, setStudentInfo] = useState();
  const [selectedStudent, setSelectedStudent] = useState();
  const [termInfo, setTermInfo] = useState();
  const [update, setUpdate] = useState();
  const [initialValues, setInitialValues] = useState();
  const [terms, setTerms] = useState([]);
  let termId;
  useEffect(() => {
    getStudentInfo();
    getTermInfo();
  }, [update]);
  async function getStudentInfo() {
    const info = await getStudents();
    console.log("stinfo", info);
    setStudentInfo(info);
    setInitialValues({
      student: info[0]._id,
    });
    setSelectedStudent(info[0]);
  }
  async function getTermInfo() {
    const info = await getTerms();
    console.log("t info", info);
    setTermInfo(info);
  }

  const selectedStudentHandler = async (e) => {
    const studentId = e.target.value;
    const student = await getStudentById(studentId);
    setSelectedStudent(student);
    createInitialValueObject(student);
  };

  const createInitialValueObject = (student) => {
    let initialValue = { ...initialValues };
    initialValue.student = student._id;
    student.terms.map(
      (term, index) =>
        (initialValue[`term${index}`] = student.terms[index].title)
    );
    setInitialValues(initialValue);
  };

  const removeFromTermHandler = async (id) => {
    const termId = id;
    const student = selectedStudent;
    console.log(student, termId);
    const data = await removeStudentFromTerm(student._id, id);
    const selected = { ...selectedStudent };
    let terms = selected.terms.map((item) => {
      if (item._id !== termId) {
        return item;
      }
    });
    terms = terms.filter((term) => term != undefined);
    selected.terms = terms;
    console.log(terms);
    setSelectedStudent(selected);
  };

  const addToTerm = async () => {
    if (termId) {
      const student = selectedStudent;
      const data = await addStudentToTerm(student._id, termId);
      const studentModel = { ...selectedStudent };
      studentModel.terms.push(data.term);
      setSelectedStudent(studentModel);
      setTerms(terms.slice(0, terms.length - 1));
      toast.success("دانشجو با موفقیت به ترم اضافه شد.");
    } else {
      toast.error("هنوز ترمی انتخاب نکرده اید.");
    }
  };

  const addTermHandler = (e) => {
    e.preventDefault();
    if (terms) setTerms([...terms, ""]);
    else setTerms([""]);
  };

  const hanldeRepeat = (term) => {
    for (let index = 0; index < selectedStudent.terms.length; index++) {
      const termId = selectedStudent.terms[index]._id;
      if (term._id === termId) {
        return false;
      }
    }
    return true;
  };
  return (
    <React.Fragment>
      {studentInfo && termInfo ? (
        <Card>
          <CardHeader>
            <CardTitle>
              <strong className="text-primary">افزودن دانشجو به ترم</strong>
            </CardTitle>
          </CardHeader>
          <CardBody>
            <Formik initialValues={initialValues} enableReinitialize={true}>
              <Form className="mt-2">
                <FormGroup>
                  <h5 htmlFor="student" className="mb-1">
                    انتخاب دانشجو
                  </h5>
                  {studentInfo && (
                    <Field
                      as="select"
                      name="student"
                      className="form-control"
                      onChange={(e) => {
                        selectedStudentHandler(e);
                      }}
                    >
                      {studentInfo.map((info, index) => (
                        <option
                          value={info._id}
                          key={index}
                          onClick={(e) => selectedStudentHandler(e)}
                        >
                          {info.fullName}
                        </option>
                      ))}
                    </Field>
                  )}
                </FormGroup>
                {selectedStudent && (
                  <FormGroup>
                    <h5 htmlFor="#" className="mb-1">
                      ترم های دانشجو
                    </h5>
                    <ol>
                      {selectedStudent.terms.length !== 0 ? (
                        selectedStudent.terms.map((info, index) => (
                          <li key={index}>
                            <Field
                              className="form-control w-50 d-inline-block"
                              name={`term${index}`}
                              type="text"
                              readOnly
                              value={info.title}
                            />

                            <Button.Ripple
                              className="d-inline-block bg-danger"
                              onClick={() => removeFromTermHandler(info._id)}
                            >
                              حذف
                            </Button.Ripple>
                          </li>
                        ))
                      ) : (
                        <p>دانشجو در هیچ ترمی شرکت نکرده است!</p>
                      )}
                      {terms.map((term, index) => (
                        <li key={index}>
                          <Field
                            as="select"
                            id="term"
                            className="form-control w-50 d-inline-block"
                            name={`term${index}`}
                            style={{ pointerEvets: "none" }}
                            onChange={() => {
                              termId = document.getElementById("term").value;
                            }}
                          >
                            {termInfo.map((term, index) => {
                              if (hanldeRepeat(term)) {
                                return (
                                  <option
                                    value={term._id}
                                    key={index}
                                    // onClick={() => {
                                    //   termId = term._id;
                                    //   console.log("clicked");
                                    // }}
                                  >
                                    {term.title}
                                  </option>
                                );
                              }
                            })}
                          </Field>
                          <Button.Ripple
                            className="d-inline-block bg-primary"
                            color="primary"
                            onClick={addToTerm}
                          >
                            تایید
                          </Button.Ripple>
                          <Button.Ripple className="d-inline-block bg-danger">
                            حذف
                          </Button.Ripple>
                        </li>
                      ))}
                    </ol>
                  </FormGroup>
                )}
                <Button.Ripple
                  color="primary"
                  type="submit"
                  onClick={(e) => addTermHandler(e)}
                >
                  افزودن ترم
                </Button.Ripple>
              </Form>
            </Formik>
            <ToastContainer position="top-center" />
          </CardBody>
        </Card>
      ) : (
        <Loading />
      )}
    </React.Fragment>
  );
};

export default AddStudentToTerm;
