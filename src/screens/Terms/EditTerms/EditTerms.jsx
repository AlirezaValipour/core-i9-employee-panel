import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Label,
  Button,
  FormGroup,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getTermById, updateTerms } from "../../../core/Services/termServices";
import { getAllCourses } from "../../../core/Services/courseServices";
import { getAllTeachers } from "../../../core/Services/employeeService";
const EditTerms = ({ match }) => {
  const [termInfo, setTermInfo] = useState();
  const [teachers, setTeachers] = useState();
  const [courses, setCourses] = useState();
  const id = match.params.id;
  useEffect(() => {
    async function getTermInfo() {
      //   const course = await getCourseForTermById(id);
      //   const { data } = await getEmployeeById(id);
      const { data } = await getAllTeachers();
      const courses = await getAllCourses();
      console.log(data.result);
      console.log(courses);
      //   console.log(data.result);
      setTeachers(data.result);
      setCourses(courses);
      const info = await getTermById(id);
      console.log(info);
      setTermInfo(info);
    }
    getTermInfo();
  }, []);
  return (
    <React.Fragment>
      {termInfo && (
        <Card>
          <CardHeader>
            <CardTitle>
              <strong className="text-primary">ویرایش ترم</strong>
            </CardTitle>
          </CardHeader>
          <CardBody>
            <Formik
              initialValues={{
                title: termInfo.title,
                cost: termInfo.cost,
                startDate: termInfo.startDate
                  .split("T")[0]
                  .replaceAll("-", "/"),
                endDate: termInfo.endDate.split("T")[0].replaceAll("-", "/"),
                capacity: termInfo.capacity,
                teacher: termInfo.teacher && termInfo.teacher._id,
                course: termInfo.course && termInfo.course._id,
              }}
              enableReinitialize={true}
              onSubmit={async (values) => {
                await updateTerms(id, values);
                toast.success("تغییرات با موفقیت اعمال شد");
                console.log(values);
              }}
            >
              <Form>
                <FormGroup>
                  <Label htmlFor="title">نام ترم</Label>

                  <Field className="form-control" name="title" type="text" />
                </FormGroup>

                <FormGroup>
                  <Label htmlFor="cost">قیمت ترم</Label>
                  <Field className="form-control" name="cost" type="text" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="startDate">تاریخ شروع ترم</Label>
                  <Field
                    className="form-control"
                    type="text"
                    name="startDate"
                  />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="endDate">تاریخ پایان ترم</Label>
                  <Field className="form-control" type="text" name="endDate" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="capacity">ظرفیت ترم</Label>
                  <Field className="form-control" type="text" name="capacity" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="teacher">مدرس ترم</Label>
                  <Field as="select" name="teacher" className="form-control">
                    {teachers &&
                      teachers.map((teacher) => (
                        <option value={teacher._id}>{teacher.fullName}</option>
                      ))}
                  </Field>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="course">نام دوره ترم</Label>
                  <Field as="select" name="course" className="form-control">
                    {courses &&
                      courses.map((course) => (
                        <option value={course._id}>{course.courseName}</option>
                      ))}
                  </Field>
                </FormGroup>
                <Button.Ripple color="primary" type="submit">
                  ثبت تغییرات
                </Button.Ripple>
              </Form>
            </Formik>
            <ToastContainer position="top-center" />
          </CardBody>
        </Card>
      )}
    </React.Fragment>
  );
};

export default EditTerms;
