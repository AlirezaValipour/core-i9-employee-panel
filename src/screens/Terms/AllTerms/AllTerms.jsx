import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import * as Icon from "react-feather";
import { Search } from "react-feather";
import Loading from "../../../components/common/Loading/Loading";
import DataTable from "react-data-table-component";
import { getTerms, deleteTerm } from "../../../core/Services/termServices";
import { normalizeDateTimePersian } from "../../../core/utils/DateHandling";

const AllTerms = () => {
  const [terms, setTerms] = useState();
  const [count, setCount] = useState();
  const [page, setPage] = useState(1);

  const [modal, setModal] = useState(false);
  const [currentId, setCurrentId] = useState();
  const [filteredData, setFilteredData] = useState([]);
  const [value, setValue] = useState("");
  const [columns, setState] = useState([
    {
      name: "نام",
      selector: "image",
      width: "12%",
      fontSize: "1rem",
      center: true,
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.title}</p>
      ),
    },
    {
      name: "استاد",
      selector: "courseName",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "0.8rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {row.teacher.fullName}
        </p>
      ),
    },
    {
      name: "ظرفیت",
      selector: "createDate",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.capacity}</p>
      ),
    },
    {
      name: "دوره ها",
      selector: "createDate",
      sortable: true,
      width: "14%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {row.course.courseName}
        </p>
      ),
    },
    {
      name: "قیمت ترم",
      selector: "createDate",
      sortable: true,
      width: "10%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <>
          <p className="text-bold-500 text-truncate mb-0">
            {row.cost + " تومان"}
          </p>
        </>
      ),
    },
    {
      name: "تاریخ شروع",
      selector: "description",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTimePersian(row.startDate.replaceAll("-", "/"))}
        </p>
      ),
    },
    {
      name: "تاریخ پایان",
      selector: "description",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTimePersian(row.endDate.replaceAll("-", "/"))}
        </p>
      ),
    },

    {
      name: "ویرایش",
      selector: "edit",
      width: "8%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <Link to={`/editTerm/${row._id}`}>
          <Icon.Edit color={"blue"} size={27} />
        </Link>
      ),
    },
    {
      name: "حذف",
      selector: "delete",
      width: "8%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <span className=" .cursor-pointer" onClick={toggle}>
          <Icon.Trash2
            color={"red"}
            style={{ cursor: "pointer" }}
            size={27}
            onClick={() => {
              setCurrentId(row._id);
              toggle();
            }}
          ></Icon.Trash2>
        </span>
      ),
    },
  ]);

  useEffect(() => {
    async function setStates() {
      const terms = await getTerms();
      console.log(terms);
      setCount(terms.length);
      setTerms(terms.reverse());
    }
    setStates();
  }, [page]);

  const toggle = () => setModal(!modal);

  const handleFilter = (e) => {
    let value = e.target.value;
    let data = terms;
    let filteredDataa = filteredData;

    if (value.length) {
      filteredDataa = data.filter((item) => {
        let startsWithCondition =
          item.title.toLowerCase().startsWith(value.toLowerCase()) ||
          item.teacher.fullName.toLowerCase().startsWith(value.toLowerCase()) ||
          item.course.courseName.toLowerCase().startsWith(value.toLowerCase());
        let includesCondition =
          item.title.toLowerCase().includes(value.toLowerCase()) ||
          item.teacher.fullName.toLowerCase().includes(value.toLowerCase()) ||
          item.course.courseName.toLowerCase().includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      setFilteredData(filteredDataa);
    }
    setValue(value);
  };

  const deleteTermHandler = async (id) => {
    const data = await deleteTerm(id);
    console.log(data.result);
    setTerms(terms.filter((item) => item._id !== id));
  };

  return (
    <>
      {terms ? (
        <>
          <Row>
            <Col>
              <Link className="btn btn-primary mb-1" to="/addTerm">
                افزودن ترم
              </Link>
            </Col>
          </Row>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">لیست ترم ها</strong>
              </CardTitle>
            </CardHeader>
            <CardBody className="rdt_Wrapper">
              <DataTable
                className="dataTable-custom"
                highlightOnHover={true}
                style={{ fontSize: "2.3rem" }}
                data={value.length ? filteredData : terms}
                columns={columns}
                noHeader
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                  <div className="d-flex flex-wrap justify-content-between">
                    <div className="position-relative has-icon-left mb-1">
                      <Input
                        value={value}
                        onChange={(e) => handleFilter(e)}
                        placeholder="جستجو ..."
                      />
                      <div className="form-control-position">
                        <Search size="15" />
                      </div>
                    </div>
                  </div>
                }
                customStyles={{
                  headCells: {
                    style: {
                      fontSize: "1.3rem",
                      textAlign: "center",
                    },
                    pagination: {
                      style: { textAlign: "center" },
                      pageButtonsStyle: {
                        border: "1px solid red",
                      },
                    },
                  },
                }}
              />
            </CardBody>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader>
                <strong>حذف کاربر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به حذف این دوره هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={() => {
                    toggle();
                    deleteTermHandler(currentId);
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
          </Card>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default AllTerms;
