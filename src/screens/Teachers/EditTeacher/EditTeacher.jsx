import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  FormGroup,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getEmployeeById,
  updateEmployeeById,
} from "../../../core/Services/employeeService";
import Loading from "../../../components/common/Loading/Loading";

const EditTeacher = ({ match }) => {
  const [teacher, setTeacher] = useState();
  const userId = match.params.id;
  console.log(teacher);
  useEffect(() => {
    async function getUserInfo(id) {
      const info = await getEmployeeById(id);
      console.log(info);
      setTeacher(info.data.result);
    }
    getUserInfo(userId);
  }, []);

  return (
    <React.Fragment>
      {teacher ? (
        <Card>
          <CardHeader>
            <CardTitle>
              <strong className="text-primary">ویرایش استاد</strong>
            </CardTitle>
          </CardHeader>
          <CardBody>
            <Formik
              initialValues={{
                fullName: teacher.fullName,
                nationalId: teacher.nationalId,
                email: teacher.email,
                phoneNumber: teacher.phoneNumber,
                birthDate: teacher.birthDate,
                address: teacher.address,
              }}
              enableReinitialize={true}
              onSubmit={async (values) => {
                await updateEmployeeById(userId, values);
                toast.success("تغییرات با موفقیت اعمال شد");
                console.log(values);
              }}
            >
              <Form>
                <FormGroup>
                  <label htmlFor="fullName">نام ونام خانوادگی</label>

                  <Field className="form-control" name="fullName" type="text" />
                </FormGroup>
                <FormGroup>
                  <label htmlFor="email">ایمیل</label>
                  <Field
                    className="form-control"
                    name="email"
                    type="email"
                    readOnly
                  />
                </FormGroup>
                <FormGroup>
                  <label htmlFor="phoneNumber">شماره همراه</label>
                  <Field
                    className="form-control"
                    name="phoneNumber"
                    type="text"
                  />
                </FormGroup>
                <FormGroup>
                  <label htmlFor="birthDate">تاریخ تولد</label>
                  <Field
                    className="form-control"
                    name="birthDate"
                    type="text"
                  />
                </FormGroup>
                <FormGroup>
                  <label htmlFor="nationalId">کد ملی</label>
                  <Field
                    readOnly
                    className="form-control"
                    name="nationalId"
                    type="text"
                  />
                </FormGroup>
                <FormGroup>
                  <label htmlFor="address">آدرس</label>
                  <Field className="form-control" name="address" type="text" />
                </FormGroup>
                <Button.Ripple color="primary" type="submit">
                  ثبت تغییرات
                </Button.Ripple>
              </Form>
            </Formik>
            <ToastContainer position="top-center" />
          </CardBody>
        </Card>
      ) : (
        <Loading />
      )}
    </React.Fragment>
  );
};

export default EditTeacher;
