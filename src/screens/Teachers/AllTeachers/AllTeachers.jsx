import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import * as Icon from "react-feather";
import Loading from "../../../components/common/Loading/Loading";
import { getAllTeachers } from "../../../core/Services/employeeService";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import {
  normalizeDateTimePersian,
  normalizeDateTime,
} from "../../../core/utils/DateHandling";
import {
  deleteEmployeeById,
  activeEmployee,
  deactiveEmployee,
} from "../../../core/Services/employeeService";
import { toast } from "react-toastify";
import DataTable from "react-data-table-component";
import { Search } from "react-feather";
const AllTeachers = () => {
  const [teachers, setTeachers] = useState();
  const [value, setValue] = useState("");
  const [filteredData, setFilteredData] = useState();
  const [modal, setModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [currentId, setCurrentId] = useState();
  const [Id, setId] = useState();
  const [status, setStatus] = useState();
  const [update, setUpdate] = useState(false);
  const [columns, setState] = useState([
    {
      name: "نام",
      selector: "name",
      width: "10%",
      fontSize: "1rem",
      center: true,
      sortable: true,
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.fullName}</p>
      ),
    },
    {
      name: "ایمیل",
      selector: "email",
      sortable: true,
      width: "14%",
      center: true,
      style: { fontSize: "0.8rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.email}</p>
      ),
    },
    {
      name: "تاریخ تولد",
      selector: "birthDate",
      sortable: true,
      minWidth: "10%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTimePersian(row.birthDate)}
        </p>
      ),
    },
    {
      name: "آدرس",
      selector: "address",
      sortable: true,
      width: "10%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.address}</p>
      ),
    },
    {
      name: "کد ملی",
      selector: "nationalId",
      sortable: true,
      width: "10%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <>
          <p className="text-bold-500 text-truncate mb-0">{row.nationalId}</p>
        </>
      ),
    },
    {
      name: "تاریخ ثبت نام",
      selector: "signUpDate",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTime(row.registerDate).persianDate}
        </p>
      ),
    },
    {
      name: "شماره تلفن",
      selector: "phoneNumber",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.phoneNumber}</p>
      ),
    },
    {
      name: "وضعیت",
      selector: "status",
      sortable: true,
      width: "6%",
      center: true,
      style: { fontSize: "1.3rem" },

      cell: (row) =>
        row.isActive ? (
          <div
            className="badge badge-success"
            style={{ cursor: "pointer" }}
            onClick={() => {
              setId(row._id);
              setDeleteModal(!deleteModal);
              setStatus("active");
            }}
          >
            فعال
          </div>
        ) : (
          <div
            className="badge badge-danger"
            style={{ cursor: "pointer" }}
            onClick={() => {
              setId(row._id);
              setDeleteModal(!deleteModal);
              setStatus("deactive");
            }}
          >
            غیر فعال
          </div>
        ),
    },
    {
      name: "ویرایش",
      selector: "edit",
      width: "6%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <Link data-tip="فعال" to={`/allTeachers/editTeacher/${row._id}`}>
          <Icon.Edit color={"blue"} size={27} />
        </Link>
      ),
    },
    {
      name: "حذف",
      selector: "delete",
      width: "6%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <span data-tip="حذف" className=" .cursor-pointer" onClick={toggle}>
          <Icon.Trash2
            color={"red"}
            style={{ cursor: "pointer" }}
            size={27}
            onClick={() => {
              setCurrentId(row._id);
              toggle();
            }}
          ></Icon.Trash2>
        </span>
      ),
    },
  ]);

  useEffect(() => {
    async function setStates() {
      const teachers = await getAllTeachers();
      console.log(teachers);
      const dataArray = teachers.data.result;
      setTeachers(dataArray);
    }
    setStates();
  }, [update]);

  const toggle = () => setModal(!modal);
  const toggleDelete = () => setDeleteModal(!deleteModal);

  const handleFilter = (e) => {
    let value = e.target.value;
    let data = teachers;
    let filteredDataa = filteredData;

    if (value.length) {
      filteredDataa = data.filter((item) => {
        let startsWithCondition = item.fullName
          .toLowerCase()
          .startsWith(value.toLowerCase());
        let includesCondition = item.fullName
          .toLowerCase()
          .includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      setFilteredData(filteredDataa);
    }
    setValue(value);
  };

  const deleteTeacherHandler = async (id) => {
    const data = await deleteEmployeeById(id);
    console.log(data.result);
    setTeachers(teachers.filter((item) => item._id !== id));
  };

  return (
    <>
      {teachers ? (
        <>
          <Row>
            <Col>
              <Link className="btn btn-primary mb-1" to="/addTerm">
                افزودن ترم
              </Link>
            </Col>
          </Row>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">لیست اساتید</strong>
              </CardTitle>
            </CardHeader>
            <CardBody className="rdt_Wrapper">
              <DataTable
                className="dataTable-custom"
                highlightOnHover={true}
                style={{ fontSize: "2.3rem" }}
                data={value.length ? filteredData : teachers}
                columns={columns}
                noHeader
                pagination
                subHeaderAlign="left"
                subHeader
                subHeaderComponent={
                  <div className="d-flex flex-wrap justify-content-between">
                    {/* <div className="add-new">
                      <Button.Ripple color="primary">
                        <Link to="/addTerm" style={{ color: "unset" }}>
                          افزودن ترم
                        </Link>
                      </Button.Ripple>
                    </div> */}
                    <div className="position-relative has-icon-left mb-1">
                      <Input
                        value={value}
                        onChange={(e) => handleFilter(e)}
                        placeholder="جستجو ..."
                      />
                      <div className="form-control-position">
                        <Search size="15" />
                      </div>
                    </div>
                  </div>
                }
                customStyles={{
                  headCells: {
                    style: {
                      fontSize: "1.3rem",
                      textAlign: "center",
                    },
                    pagination: {
                      style: { textAlign: "center" },
                      pageButtonsStyle: {
                        border: "1px solid red",
                      },
                    },
                  },
                }}
              />
            </CardBody>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader>
                <strong>حذف کاربر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به حذف این کاربر هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={() => {
                    toggle();
                    deleteTeacherHandler(currentId);
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={deleteModal} toggle={toggleDelete}>
              <ModalHeader>
                <strong>تغییر وضعیت کاربر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به تغییر وضعیت این کاربر هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={async () => {
                    toggleDelete();
                    if (status === "active") {
                      async function deactive(Id) {
                        return await deactiveEmployee(Id);
                      }
                      const data = await deactive(Id);
                      console.log(data);
                      setUpdate(!update);
                    }
                    if (status === "deactive") {
                      async function active(Id) {
                        return await activeEmployee(Id);
                      }
                      const data = await active(Id);
                      console.log(data);
                      setUpdate(!update);
                    }
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggleDelete}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
          </Card>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default AllTeachers;
