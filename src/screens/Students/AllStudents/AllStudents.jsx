import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import * as Icon from "react-feather";
import { Search } from "react-feather";
import Loading from "../../../components/common/Loading/Loading";
import {
  deleteStudentById,
  getStudents,
  getStudentsForPagination,
  activeStudent,
  deactiveStudent,
} from "../../../core/Services/studentServices";
import {
  normalizeDateTime,
  normalizeDateTimePersian,
} from "../../../core/utils/DateHandling";
import DataTable from "react-data-table-component";

const AllStudents = () => {
  const [students, setStudents] = useState();
  const [count, setCount] = useState();
  const [page, setPage] = useState(1);
  const [modal, setModal] = useState(false);
  const [currentId, setCurrentId] = useState();
  const [filteredData, setFilteredData] = useState([]);
  const [value, setValue] = useState("");
  const [Id, setId] = useState();
  const [statusModal, setStatusModal] = useState(false);
  const [status, setStatus] = useState();
  const [update, setUpdate] = useState(false);
  const [columns, setState] = useState([
    {
      name: "نام",
      selector: "image",
      width: "14%",
      fontSize: "1rem",
      center: true,
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.fullName}</p>
      ),
    },
    {
      name: "ایمیل",
      selector: "courseName",
      sortable: true,
      width: "18%",
      center: true,
      style: { fontSize: "0.8rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.email}</p>
      ),
    },
    {
      name: "تاریخ تولد",
      selector: "createDate",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">
          {normalizeDateTimePersian(row.birthDate)}
        </p>
      ),
    },
    {
      name: "شماره تلفن",
      selector: "description",
      sortable: true,
      width: "14%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.phoneNumber}</p>
      ),
    },
    {
      name: "کد ملی",
      selector: "nationalId",
      sortable: true,
      width: "14%",
      center: true,
      style: { fontSize: "1rem" },
      cell: (row) => (
        <p className="text-bold-500 text-truncate mb-0">{row.nationalId}</p>
      ),
    },
    {
      name: "وضعیت",
      selector: "status",
      sortable: true,
      width: "12%",
      center: true,
      style: { fontSize: "1.3rem" },
      cell: (row) =>
        row.isActive ? (
          <div
            className="badge badge-success"
            style={{ cursor: "pointer" }}
            onClick={() => {
              setId(row._id);
              setStatusModal(!statusModal);
              setStatus("active");
            }}
          >
            فعال
          </div>
        ) : (
          <div
            className="badge badge-danger"
            style={{ cursor: "pointer" }}
            onClick={() => {
              setId(row._id);
              setStatusModal(!statusModal);
              setStatus("deactive");
            }}
          >
            غیر فعال
          </div>
        ),
    },
    {
      name: "ویرایش",
      selector: "edit",
      width: "8%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <Link data-tip="فعال" to={`/editStudent/${row._id}`}>
          <Icon.Edit color={"blue"} size={27} />
        </Link>
      ),
    },
    {
      name: "حذف",
      selector: "delete",
      width: "8%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <span data-tip="حذف" className=" .cursor-pointer" onClick={toggle}>
          <Icon.Trash2
            color={"red"}
            style={{ cursor: "pointer" }}
            size={27}
            onClick={() => {
              console.log(row._id);
              setCurrentId(row._id);
              toggle();
            }}
          ></Icon.Trash2>
        </span>
      ),
    },
  ]);

  useEffect(() => {
    async function setStates() {
      const students = await getStudents();
      console.log(students);
      setCount(students.length);
      setStudents(students.reverse());
    }
    setStates();
  }, [update]);

  const toggle = () => setModal(!modal);
  const statusToggle = () => setStatusModal(!statusModal);

  const handleFilter = (e) => {
    let value = e.target.value;
    console.log("run", value);
    let data = students;
    let filteredDataa = filteredData;

    if (value.length) {
      filteredDataa = data.filter((item) => {
        let startsWithCondition =
          item.fullName.toLowerCase().startsWith(value.toLowerCase()) ||
          item.email.toLowerCase().startsWith(value.toLowerCase()) ||
          item.nationalId.toLowerCase().startsWith(value.toLowerCase()) ||
          item.phoneNumber.toLowerCase().startsWith(value.toLowerCase());
        let includesCondition =
          item.fullName.toLowerCase().includes(value.toLowerCase()) ||
          item.email.toLowerCase().includes(value.toLowerCase()) ||
          item.nationalId.toLowerCase().includes(value.toLowerCase()) ||
          item.phoneNumber.toLowerCase().includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      setFilteredData(filteredDataa);
    }
    setValue(value);
  };

  const deleteStudentHandler = async (id) => {
    const data = await deleteStudentById(id);
    console.log(data.result);
    setStudents(students.filter((item) => item._id !== id));
  };

  return (
    <>
      {students ? (
        <>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">لیست دانشجویان</strong>
              </CardTitle>
            </CardHeader>
            <CardBody className="rdt_Wrapper">
              <DataTable
                className="dataTable-custom"
                highlightOnHover={true}
                style={{ fontSize: "2.3rem" }}
                data={value.length ? filteredData : students}
                columns={columns}
                noHeader
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                  <div className="d-flex flex-wrap justify-content-between">
                    <div className="position-relative has-icon-left mb-1">
                      <Input
                        value={value}
                        onChange={(e) => handleFilter(e)}
                        placeholder="جستجو ..."
                      />
                      <div className="form-control-position">
                        <Search size="15" />
                      </div>
                    </div>
                  </div>
                }
                customStyles={{
                  headCells: {
                    style: {
                      fontSize: "1.3rem",
                      textAlign: "center",
                    },
                    pagination: {
                      style: { textAlign: "center" },
                      pageButtonsStyle: {
                        border: "1px solid red",
                      },
                    },
                  },
                }}
              />
            </CardBody>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader>
                <strong>حذف کاربر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به حذف این دانشجو هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={() => {
                    toggle();
                    deleteStudentHandler(currentId);
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
            <Modal isOpen={statusModal} toggle={statusToggle}>
              <ModalHeader>
                <strong>تغییر وضعیت کاربر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به تغییر وضعیت این کاربر هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={async () => {
                    statusToggle();
                    if (status === "active") {
                      async function deactive(Id) {
                        return await deactiveStudent(Id);
                      }
                      const data = await deactive(Id);
                      console.log(data);
                      setUpdate(!update);
                    }
                    if (status === "deactive") {
                      async function active(Id) {
                        return await activeStudent(Id);
                      }
                      const data = await active(Id);
                      console.log(data);
                      setUpdate(!update);
                    }
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={statusToggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
          </Card>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default AllStudents;
