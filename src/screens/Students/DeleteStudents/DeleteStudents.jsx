import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { deleteStudentById } from "../../../core/Services/studentServices";
const DeleteStudents = ({ match }) => {
  const id = match.params.id;
  useEffect(() => {
    async function deleteStudent() {
      await deleteStudentById(id);
    }
    deleteStudent();
  }, []);
  return (
    <React.Fragment>
      <Redirect to="/allstudents" />
    </React.Fragment>
  );
};

export default DeleteStudents;
