import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  FormGroup,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getStudentById,
  updateStudent,
} from "../../../core/Services/studentServices";
import Loading from "../../../components/common/Loading/Loading";

const EditStudents = ({ match }) => {
  const [studentInfo, setStudentInfo] = useState();
  const id = match.params.id;
  useEffect(() => {
    async function getStudentInfo() {
      const info = await getStudentById(id);
      console.log(info);
      setStudentInfo(info);
    }
    getStudentInfo();
  }, []);
  return studentInfo ? (
    <React.Fragment>
      <Card>
        <CardHeader>
          <CardTitle>
            <strong className="text-primary">ویرایش دانشجو</strong>
          </CardTitle>
        </CardHeader>
        <CardBody>
          <Formik
            initialValues={{
              fullName: studentInfo.fullName,
              nationalId: studentInfo.nationalId,
              email: studentInfo.email,
              phoneNumber: studentInfo.phoneNumber,
              birthDate: studentInfo.birthDate,
            }}
            enableReinitialize={true}
            onSubmit={async (values) => {
              await updateStudent(id, values);
              toast.success("تغییرات با موفقیت اعمال شد");
              console.log(values);
            }}
          >
            <Form>
              <FormGroup>
                <label htmlFor="fullName">نام ونام خانوادگی</label>

                <Field className="form-control" name="fullName" type="text" />
              </FormGroup>

              <FormGroup>
                <label htmlFor="email">ایمیل</label>
                <Field
                  className="form-control"
                  name="email"
                  type="email"
                  readOnly
                />
              </FormGroup>
              <FormGroup>
                <label htmlFor="phoneNumber">شماره همراه</label>
                <Field
                  className="form-control"
                  name="phoneNumber"
                  type="text"
                />
              </FormGroup>
              <FormGroup>
                <label htmlFor="birthDate">تاریخ تولد</label>
                <Field className="form-control" name="birthDate" type="text" />
              </FormGroup>
              <FormGroup>
                <label htmlFor="nationalId">کد ملی</label>
                <Field
                  className="form-control"
                  name="nationalId"
                  type="text"
                  readOnly
                />
              </FormGroup>

              <Button.Ripple color="primary" type="submit">
                ثبت تغییرات
              </Button.Ripple>
            </Form>
          </Formik>
          <ToastContainer position="top-center" />
        </CardBody>
      </Card>
    </React.Fragment>
  ) : (
    <Loading />
  );
};

export default EditStudents;
