import React, { useEffect } from "react";
import {
  deactiveStudent,
  getStudentById,
} from "../../../core/Services/studentServices";
const DeactiveStudent = ({ match }) => {
  const id = match.params.id;
  useEffect(() => {
    async function deactive() {
      const student = await getStudentById(id);
      const deactiveStu = await deactiveStudent(id, student);
      window.location.href = "/allstudents";
      console.log(deactiveStu);
    }
    deactive();
  }, []);
  return null;
};

export default DeactiveStudent;
