import React, { useEffect } from "react";
import {
  activeStudent,
  getStudentById,
} from "../../../core/Services/studentServices";
const ActiveStudent = ({ match }) => {
  const id = match.params.id;
  useEffect(() => {
    async function active() {
      const student = await getStudentById(id);
      console.log(student);
      const activeStu = await activeStudent(id, student);
      window.location.href = "/allstudents";
      console.log(activeStu);
    }
    active();
  }, []);
  return null;
};

export default ActiveStudent;
