import React, { useState, useEffect } from "react";
import {
  Col,
  Row,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Button,
  FormGroup,
  FormText,
  Label,
  Input,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createNewsArticle } from "../../../core/Services/newsService";
import http from "../../../core/Services/httpServices";

const AddNews = () => {
  const [category, setCategory] = useState("news");
  let imageUrl;
  return (
    <React.Fragment>
      <Card>
        <CardHeader>
          <CardTitle>
            <strong className="text-primary">افزودن خبر و مقاله</strong>
          </CardTitle>
        </CardHeader>
        <CardBody>
          <Formik
            initialValues={{
              category: category,
              title: "",
              text: "",
              image: "",
            }}
            enableReinitialize={true}
            onSubmit={async (values, { resetForm }) => {
              let data = { ...values };
              data.image = imageUrl;
              await createNewsArticle(data);
              toast.success("تغییرات با موفقیت اعمال شد");
              console.log(values);
              resetForm();
            }}
          >
            <Form>
              <FormGroup>
                <Label htmlFor="category">دسته بندی</Label>
                <Field as="select" name="category" className="form-control">
                  <option value="news">خبر</option>
                  <option value="article">مقاله</option>
                </Field>
              </FormGroup>

              <FormGroup>
                <Label htmlFor="title">عنوان</Label>
                <Field className="form-control" name="title" type="text" />
              </FormGroup>

              <FormGroup>
                <Label htmlFor="text">متن</Label>
                <Field as="textarea" className="form-control" name="text" />
              </FormGroup>

              <FormGroup>
                <Label className="mr-1" for="image">
                  آپلود عکس
                </Label>
                <Field
                  onChange={async (e) => {
                    toast.info("لطفا منتظر آپلود شدن عکس باشید :)");
                    const formData = new FormData();
                    formData.append("image", e.target.files[0]);
                    const res = await http.uploadFile(formData);
                    console.log(res);
                    imageUrl = res.data.result;
                    if (res.data.message[0].message)
                      toast.success("عکس مورد نظر آپلود شد :)");
                  }}
                  type="file"
                  name="image"
                />
                <FormText className="mt-1" color="danger">
                  عکس موردنظر خود را آپلود کنید
                </FormText>
              </FormGroup>

              <Button.Ripple color="primary" type="submit">
                ثبت تغییرات
              </Button.Ripple>
            </Form>
          </Formik>
          <ToastContainer position="top-center" />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

export default AddNews;
