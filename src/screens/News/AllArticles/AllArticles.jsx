import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
  Table,
} from "reactstrap";
import Loading from "../../../components/common/Loading/Loading";
import DataTable from "react-data-table-component";
import {
  getAllNews,
  deleteNewsAndArticle,
  getInfoByCategory,
} from "../../../core/Services/newsService";
import { Search } from "react-feather";
import { WordSeperator } from "../../../core/utils/StringManipulation";
import * as Icon from "react-feather";
import {
  normalizeDateTime,
  dateDifference,
  dateDifference2,
} from "../../../core/utils/DateHandling";
const AllArticles = () => {
  const [news, setNews] = useState();
  const [modal, setModal] = useState(false);
  const [currentId, setCurrentId] = useState();
  const [filteredData, setFilteredData] = useState([]);
  const [value, setValue] = useState("");
  const [columns, setState] = useState([
    {
      name: "تصویر مقاله",
      selector: "image",
      width: "28%",
      fontSize: "1rem",
      center: true,
      cell: (row) => (
        <div className="h-100 p-0 w-100">
          <img
            className="text-bold-500 mb-0 w-100"
            style={{ height: "200px", padding: "5px" }}
            src={row.image}
          />
        </div>
      ),
    },
    {
      name: "نام مقاله",
      selector: "title",
      sortable: true,
      width: "17%",
      center: true,
      style: { fontSize: "1.1rem" },
      cell: (row) => (
        <div className="d-flex flex-xl-row flex-column align-items-xl-center align-items-start py-xl-0 py-1">
          {row.title}
        </div>
      ),
    },

    {
      name: "متن مقاله",
      selector: "text",
      sortable: true,
      width: "28%",
      center: true,
      style: { fontSize: "1rem" },

      cell: (row) => <div>{WordSeperator(row.text, 20)}</div>,
    },

    {
      name: "ویرایش",
      selector: "edit",
      width: "10%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <Link to={`/editNewsArticle/${row._id}`}>
          <Icon.Edit color={"blue"} size={26} />
        </Link>
      ),
    },
    {
      name: "حذف",
      selector: "delete",
      width: "10%",
      center: true,
      style: {
        fontSize: "1rem",
        textAlign: "center",
      },

      cell: (row) => (
        <span className=" .cursor-pointer" onClick={toggle}>
          <Icon.Trash2
            color={"red"}
            style={{ cursor: "pointer" }}
            size={26}
            onClick={() => {
              console.log(row._id);
              setCurrentId(row._id);
              toggle();
            }}
          ></Icon.Trash2>
        </span>
      ),
    },
  ]);

  useEffect(() => {
    async function setStates() {
      const news = await getInfoByCategory("article");
      setNews(news.reverse());
      console.log(news.reverse());
    }
    setStates();
  }, []);

  const toggle = () => setModal(!modal);

  const deleteNewsArticleHandler = async (id) => {
    const data = await deleteNewsAndArticle(id);
    console.log(data.result);
    setNews(news.filter((item) => item._id !== id));
  };

  const ExpandableRow = ({ data }) => {
    return (
      <>
        <img className="d-block w-50 mx-auto my-1" src={data.image} />
        <strong className="text-danger">توضیحات بیشتر:</strong>
        <div>{data.text}</div>
      </>
    );
  };
  const handleFilter = (e) => {
    let value = e.target.value;
    let data = news;
    let filteredDataa = filteredData;

    if (value.length) {
      filteredDataa = data.filter((item) => {
        let startsWithCondition =
          item.title.toLowerCase().startsWith(value.toLowerCase()) ||
          item.text.toLowerCase().startsWith(value.toLowerCase()) ||
          item.category.toLowerCase().startsWith(value.toLowerCase());
        let includesCondition =
          item.title.toLowerCase().includes(value.toLowerCase()) ||
          item.text.toLowerCase().includes(value.toLowerCase()) ||
          item.category.toLowerCase().includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      setFilteredData(filteredDataa);
    }
    setValue(value);
  };

  return (
    <>
      {news ? (
        <>
          <Row>
            <Col>
              <Link className="btn btn-primary mb-1" to="/addNewsArticle">
                افزودن خبر و مقاله
              </Link>
            </Col>
          </Row>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">لیست مقالات</strong>
              </CardTitle>
            </CardHeader>
            <CardBody className="rdt_Wrapper">
              <DataTable
                className="dataTable-custom"
                highlightOnHover={true}
                style={{ fontSize: "1rem" }}
                data={value.length ? filteredData : news}
                columns={columns}
                noHeader
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                  <div className="d-flex flex-wrap justify-content-between">
                    <div className="position-relative has-icon-left mb-1">
                      <Input
                        value={value}
                        onChange={(e) => handleFilter(e)}
                        placeholder="جستجو ..."
                      />
                      <div className="form-control-position">
                        <Search size="15" />
                      </div>
                    </div>
                  </div>
                }
                customStyles={{
                  headCells: {
                    style: {
                      fontSize: "1.3rem",
                      textAlign: "center",
                    },
                    pagination: {
                      style: { textAlign: "center" },
                      pageButtonsStyle: {
                        border: "1px solid red",
                      },
                    },
                  },
                }}
                expandableRows
                expandOnRowClicked
                expandableRowsComponent={<ExpandableRow />}
                expandableRowExpanded={(row) => row.text.expandableRowExpanded}
              />
            </CardBody>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader>
                <strong>حذف خبر</strong>
              </ModalHeader>
              <ModalBody>آیا مطمئن به حذف این مقاله هستید؟</ModalBody>
              <ModalFooter>
                <Button
                  color="primary"
                  onClick={() => {
                    toggle();
                    deleteNewsArticleHandler(currentId);
                  }}
                >
                  بله
                </Button>
                <Button color="danger" onClick={toggle}>
                  خیر
                </Button>
              </ModalFooter>
            </Modal>
          </Card>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default AllArticles;
