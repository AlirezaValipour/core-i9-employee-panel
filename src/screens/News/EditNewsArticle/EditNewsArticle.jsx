import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  FormGroup,
  Label,
  FormText,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getNewsById,
  updateNewsArticle,
} from "../../../core/Services/newsService";
import http from "../../../core/Services/httpServices";
import Loading from "../../../components/common/Loading/Loading";
const EditNewsArticle = ({ match }) => {
  const [newsAndArticleInfo, setNewsAndArticleInfo] = useState({});
  const [image, setImage] = useState();
  const id = match.params.id;

  useEffect(() => {
    async function getNewsInfo() {
      const info = await getNewsById(id);
      console.log(info);
      setNewsAndArticleInfo(info);
      console.log(newsAndArticleInfo);
      setImage(info.image);
    }
    getNewsInfo();
  }, []);

  const imageUpload = async (e) => {
    const formData = new FormData();
    formData.append("image", e.target.files[0]);
    const res = await http.uploadFile(formData);
    toast.success("عکس مورد نظر آپلود شد.");
    console.log(res.data.result);
    setImage(res.data.result);
  };

  const submitHandler = async (values) => {
    let data = { ...values };
    data.image = image;
    const res = await updateNewsArticle(id, data);
    console.log(res);
    toast.success("تغییرات با موفقیت اعمال شد");
    console.log(values);
  };
  return (
    <>
      {newsAndArticleInfo ? (
        <React.Fragment>
          <Card>
            <CardHeader>
              <CardTitle>
                <strong className="text-primary">ویرایش</strong>
              </CardTitle>
            </CardHeader>
            <CardBody>
              <img
                src={image}
                className="w-75 mx-auto img-fluid d-block"
                alt="Course Picture"
              />
              <Formik
                initialValues={{
                  category: newsAndArticleInfo.category,
                  title: newsAndArticleInfo.title,
                  text: newsAndArticleInfo.text,
                }}
                enableReinitialize={true}
                onSubmit={(values) => {
                  submitHandler(values);
                }}
              >
                <Form>
                  <FormGroup>
                    <Label htmlFor="category">دسته بندی</Label>
                    <Field as="select" name="category" className="form-control">
                      <option value="news">خبر</option>
                      <option value="article">مقاله</option>
                    </Field>
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="title">عنوان</Label>
                    <Field className="form-control" name="title" type="text" />
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="text">متن</Label>
                    <Field as="textarea" className="form-control" name="text" />
                  </FormGroup>

                  <FormGroup>
                    <Label className="mr-1" for="image">
                      آپلود عکس
                    </Label>
                    <Field
                      type="file"
                      name="image"
                      onChange={(e) => imageUpload(e)}
                    />
                    <FormText className="mt-1" color="danger">
                      عکس موردنظر خود را آپلود کنید
                    </FormText>
                  </FormGroup>

                  <Button.Ripple color="primary" type="submit">
                    ثبت تغییرات
                  </Button.Ripple>
                </Form>
              </Formik>
              <ToastContainer position="top-center" />
            </CardBody>
          </Card>
        </React.Fragment>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default EditNewsArticle;
